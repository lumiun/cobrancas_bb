<?php

namespace Lumiun\CobrancasBB\Test\Classes;

use DateInterval;
use DateTimeImmutable;
use PHPUnit\Framework\TestCase;
use Lumiun\CobrancasBB\Middleware\ApiAuth;

class ApiAuthTest extends TestCase
{
    protected string $environment;
    protected string $gwDevAppKey;
    protected string $clientId;
    protected string $clientSecret;

    protected ApiAuth $auth;

    public function setUp(): void
    {
        $this->environment = 'TEST';

        $this->gwDevAppKey = 'd27bd7790fffab801367e17d60050156b9e1a5bd';

        $this->clientId = 'eyJpZCI6IiIsImNvZGlnb1B1YmxpY2Fkb3IiOjAsImNvZGlnb1NvZnR3YXJlIjoxNDMwMiwic2VxdWVuY2lhbEluc3';
        $this->clientId .= 'RhbGFjYW8iOjF9';

        $this->clientSecret = 'eyJpZCI6IjdiN2E5YTMtODliMS00MzI3LWJjZmQtYWE0YWRhNzYzIiwiY29kaWdvUHVibGljYWRvciI6MCwiY2';
        $this->clientSecret .= '9kaWdvU29mdHdhcmUiOjE0MzAyLCJzZXF1ZW5jaWFsSW5zdGFsYWNhbyI6MSwic2VxdWVuY2lhbENyZWRlbmNp';
        $this->clientSecret .= 'YWwiOjEsImFtYmllbnRlIjoiaG9tb2xvZ2FjYW8iLCJpYXQiOjE2MTY3MDMzODE4OTh9';

        // PRÉ-ARRANGE
        $this->auth = new ApiAuth(
            $this->environment,
            $this->gwDevAppKey,
            $this->clientId,
            $this->clientSecret
        );
    }

    /**
     * In this test, should make a request to the authentication api and return a valid token.
     * It must verify that the received token is the same one contained in the cache folder.
     */
    public function testGetAValidAccessToken()
    {
        // ACT
        $token = $this->auth->getAccessToken();

        // ASSERT -> accessToken
        self::assertIsString(
            $token,
            'O metodo "getAccessToken()" nao retornou uma string.'
        );
    }

    /**
     * In this test, must receive a Client Guzzle header.
     */
    public function testGetGuzzleHeader()
    {
        // ACT
        $guzzleHeader = $this->auth->getHeader();
        $token = $this->auth->getAccessToken();

        // ASSERT
        self::assertIsArray(
            $guzzleHeader,
            'O metodo "getHeader()" nao retornou um array.'
        );

        self::assertArrayHasKey(
            'headers',
            $guzzleHeader,
            'O array retornado por "getHeader()" nao possui a chave "headers".'
        );

        self::assertArrayHasKey(
            'Authorization',
            $guzzleHeader['headers'],
            'O array "headers" contido no retorno de "getHeader()" nao possui a chave "Authorization".'
        );
        self::assertEquals(
            'Bearer ' . $token,
            $guzzleHeader['headers']['Authorization'],
            'O valor do header "Authorization" nao e compativel com o token recebido pela classe ApiAuth.'
        );

        self::assertArrayHasKey(
            'Content-Type',
            $guzzleHeader['headers'],
            'O array "headers" contido no retorno de "getHeader()" nao possui a chave "Content-Type".'
        );
        self::assertEquals(
            'application/json',
            $guzzleHeader['headers']['Content-Type'],
            'O valor do header "Content-Type" nao e compativel com "application/json".'
        );

        self::assertArrayHasKey(
            'verify',
            $guzzleHeader,
            'O array retornado por "getHeader()" nao possui a chave "verify".'
        );
        self::assertEquals(
            false,
            $guzzleHeader['verify'],
            'O valor de "verify" deve ser "false".'
        );
    }

    /**
     * In this test, should receive the GwDevApp Key
     */
    public function testGetDevAppKey()
    {
        // ACT
        $gwDevAppKey = $this->auth->getAppKey();

        // ASSERT
        self::assertIsString(
            $gwDevAppKey,
            'O metodo "getAppKey()" nao retornou uma string.'
        );

        self::assertNotEmpty(
            $gwDevAppKey,
            'O metodo "getAppKey()" retornou uma string vazia.'
        );

        self::assertEquals(
            $this->gwDevAppKey,
            $gwDevAppKey,
            'A DEV_APP_KEY retornada pelo metodo "getAppKey()" nao e compativel com a key de Mock.'
        );
    }

    /**
     * Should get a token that is cached and verify that it is still valid.
     */
    public function testCheckIfTokenIsValid()
    {
        // ARRANGE
        $this->auth->getAccessToken();
        $realFileName = mb_substr(__DIR__, 0, strripos(__DIR__, '/') + 1) . '../src/Storage/cache/token.json';
        $mockFileName = mb_substr(__DIR__, 0, strripos(__DIR__, '/') + 1) . '/Mock/cache/token.json';

        if (!file_exists($realFileName)) {
            self::fail(
                'O arquivo de cache nao foi criado durante a execucao do metodo "getAccessToken()".'
            );
        } elseif (!$realCachedContents = file_get_contents($realFileName)) {
            self::fail(
                'Nao foi possivel carregar os dados do arquivo de cache.'
            );
        }

        if (!file_put_contents($mockFileName, $realCachedContents)) {
            self::fail(
                'Nao foi possivel gravar o arquivo de Mock.'
            );
        } elseif (!$mockContents = file_get_contents($mockFileName)) {
            self::fail(
                'Nao foi possivel carregar os dados do arquivo de Mock.'
            );
        }

        $token = json_decode($mockContents, true);

        if (!is_array($token)) {
            self::fail(
                'Nao foi possivel converter os dados do arquivo de Mock em um array.'
            );
        }

        if (!isset($token['expires_in'])) {
            self::fail(
                'O array contido no arquivo de mock nao contem a chave "expires_in".'
            );
        }

        // ACT
        $checkToken = $this->auth->checkIfTokenIsValid($token['expires_in']);

        // ASSERT
        self::assertEquals(
            true,
            $checkToken,
            'O token esperado deveria estar valido, porem o token em cache e invalido.'
        );
    }

    /**
     * Should get a token that is cached and verify that it is no longer valid.
     */
    public function testCheckIfTokenIsNoLongerValid()
    {
        // ARRANGE
        $this->auth->getAccessToken();
        $realFileName = mb_substr(__DIR__, 0, strripos(__DIR__, '/') + 1) . '../src/Storage/cache/token.json';
        $mockFileName = mb_substr(__DIR__, 0, strripos(__DIR__, '/') + 1) . '/Mock/cache/invalidToken.json';

        if (!file_exists($realFileName)) {
            self::fail(
                'O arquivo de cache nao foi criado durante a execucao do metodo "getAccessToken()".'
            );
        } elseif (!$realCachedContents = file_get_contents($realFileName)) {
            self::fail(
                'Nao foi possivel carregar os dados do arquivo de cache.'
            );
        }

        $realCachedContents = json_decode($realCachedContents, true);

        $now = new DateTimeImmutable('now');
        $lessAnHour = $now->sub(new DateInterval('PT1H'));

        $realCachedContents['expires_in'] = $lessAnHour->getTimestamp();

        $realCachedContents = json_encode($realCachedContents);

        if (!file_put_contents($mockFileName, $realCachedContents)) {
            self::fail(
                'Nao foi possivel gravar o arquivo de Mock.'
            );
        } elseif (!$mockContents = file_get_contents($mockFileName)) {
            self::fail(
                'Nao foi possivel carregar os dados do arquivo de Mock.'
            );
        }

        $token = json_decode($mockContents, true);

        if (!is_array($token)) {
            self::fail(
                'Nao foi possivel converter os dados do arquivo de Mock em um array.'
            );
        }

        if (!isset($token['expires_in'])) {
            self::fail(
                'O array contido no arquivo de mock nao contem a chave "expires_in".'
            );
        }

        // ACT
        $checkToken = $this->auth->checkIfTokenIsValid($token['expires_in']);

        // ASSERT
        self::assertEquals(
            false,
            $checkToken,
            'O token esperado deveria estar invalido, porem o metodo "checkIfTokenIsValid()" retornou que ele e valido.'
        );
    }

    /**
     * Should clear the cache and check if the folder is empty.
     */
    public function testClearTokenCache()
    {
        // ARRANGE
        $this->auth->getAccessToken();
        $realFileName = mb_substr(__DIR__, 0, strripos(__DIR__, '/') + 1) . '../src/Storage/cache/token.json';

        if (!file_exists($realFileName)) {
            self::fail(
                'O arquivo de cache nao foi criado durante a execucao do metodo "getAccessToken()".'
            );
        }

        // ACT
        $cacheCleared = $this->auth->clearCache();

        // ASSERT
        self::assertFileDoesNotExist(
            $realFileName,
            'O arquivo de cache nao foi excluido durante a limpeza de cache.'
        );

        self::assertEquals(
            true,
            $cacheCleared,
            'O retorno esperado do metodo "clearCache()" era "true", porem retornou "false".'
        );
    }
}
