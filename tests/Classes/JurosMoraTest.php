<?php

namespace Lumiun\CobrancasBB\Test\Classes;

use PHPUnit\Framework\TestCase;
use Lumiun\CobrancasBB\Classes\JurosMora;

class JurosMoraTest extends TestCase
{
    public function testJurosMoraSemParametrosToArray()
    {
        // ARRANGE
        $juros = new JurosMora([]);

        // ACT
        $array = $juros->toArray();
        $formatted = $juros->formatedData();

        // ASSERT -> toArray
        self::assertArrayHasKey('tipo', $array);
        self::assertArrayHasKey('porcentagem', $array);
        self::assertEquals((int) 2, $array['tipo']);
        self::assertEquals((float) 1, $array['porcentagem']);

        // ASSERT -> formatedData
        self::assertArrayHasKey('tipo', $formatted);
        self::assertArrayHasKey('juros', $formatted);
        self::assertEquals((int) 2, $formatted['tipo']);
        self::assertEquals((string) '0,033%', $formatted['juros']);
    }

    public function testJurosMoraPorcentagemToArray()
    {
        // ARRANGE
        $juros = new JurosMora([
            'tipo' => 2,
            'porcentagem' => 5,
        ]);

        // ACT
        $array = $juros->toArray();
        $formatted = $juros->formatedData();

        // ASSERT -> toArray
        self::assertArrayHasKey('tipo', $array);
        self::assertArrayHasKey('porcentagem', $array);
        self::assertEquals((int) 2, $array['tipo']);
        self::assertEquals((float) 5, $array['porcentagem']);

        // ASSERT -> formatedData
        self::assertArrayHasKey('tipo', $formatted);
        self::assertArrayHasKey('juros', $formatted);
        self::assertEquals((int) 2, $formatted['tipo']);
        self::assertEquals((string) '0,167%', $formatted['juros']);
    }

    public function testJurosMoraValorToArray()
    {
        // ARRANGE
        $juros = new JurosMora([
            'tipo' => 1,
            'valor' => 5,
        ]);

        // ACT
        $array = $juros->toArray();
        $formatted = $juros->formatedData();

        // ASSERT -> toArray
        self::assertArrayHasKey('tipo', $array);
        self::assertArrayHasKey('valor', $array);
        self::assertEquals((int) 1, $array['tipo']);
        self::assertEquals((float) 5, $array['valor']);

        // ASSERT -> formatedData
        self::assertArrayHasKey('tipo', $formatted);
        self::assertArrayHasKey('juros', $formatted);
        self::assertEquals((int) 1, $formatted['tipo']);
        self::assertEquals((string) 'R$ 5,00', $formatted['juros']);
    }

    public function testJurosMoraDispensarToArray()
    {
        // ARRANGE
        $juros = new JurosMora([
            'tipo' => 0,
        ]);

        // ACT
        $array = $juros->toArray();
        $formatted = $juros->formatedData();

        // ASSERT -> toArray
        self::assertArrayHasKey('tipo', $array);
        self::assertEquals((int) 0, $array['tipo']);

        // ASSERT -> formatedData
        self::assertArrayHasKey('tipo', $formatted);
        self::assertEquals((int) 0, $formatted['tipo']);
    }

    public function testJurosMoraIsentoToArray()
    {
        // ARRANGE
        $juros = new JurosMora([
            'tipo' => 3,
        ]);

        // ACT
        $array = $juros->toArray();
        $formatted = $juros->formatedData();

        // ASSERT -> toArray
        self::assertArrayHasKey('tipo', $array);
        self::assertEquals((int) 3, $array['tipo']);

        // ASSERT -> formatedData
        self::assertArrayHasKey('tipo', $formatted);
        self::assertArrayHasKey('juros', $formatted);
        self::assertEquals((int) 3, $formatted['tipo']);
        self::assertEquals((string) 'cobrança isenta de juros', $formatted['juros']);
    }
}
