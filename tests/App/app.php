<?php

require_once __DIR__ . '/../../vendor/autoload.php';

use Lumiun\CobrancasBB\Test\App\Teste;

$test = new Teste();

switch ($_GET['function']) {
    case 1:
        echo '<a href="/">Voltar</a>';
        echo '<pre>';
        var_dump($test->registerJson());
        echo '</pre>';
        break;

    case 2:
        echo '<a href="/">Voltar</a>';
        echo '<pre>';
        var_dump($test->registerMany());
        echo '</pre>';
        break;

    case 3:
        $result = $test->registerHtml();
        $html = $result['html'];
        unset($result['html']);
        echo '<a href="/">Voltar</a>';
        echo '<pre>';
        var_dump($result);
        echo '</pre>';

        echo $html;

        break;

    case 4:
        echo '<a href="/">Voltar</a>';
        echo '<pre>';
        var_dump($test->getList());
        echo '</pre>';
        break;

    case 5:
        echo '<a href="/">Voltar</a>';
        echo $test->detail($_GET['nossoNumero'], $_GET['json'] ?? false);
        break;

    case 6:
        echo '<a href="/">Voltar</a>';
        echo '<pre>';
        var_dump($test->destroy($_GET['nossoNumero']));
        echo '</pre>';
        break;

    default:
        # code...
        break;
}
