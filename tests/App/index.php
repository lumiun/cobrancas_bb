<?php

    $phpVersion = phpversion();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>PHP: <?= $phpVersion ?></h1>
    <ul>
        <li><a href="app.php?function=1">Registrar -> Json</a></li>
        <li><a href="app.php?function=2">Registrar -> Muitos</a></li>
        <li><a href="app.php?function=3">Registrar -> HTML</a></li>
        <li><a href="app.php?function=4">Listar</a></li>
        <li><a href="app.php?function=5&nossoNumero=00031285576417700720">Detalhar -> Json</a></li>
        <li><a href="app.php?function=5&nossoNumero=00031285570000000066&json=true">Detalhar -> HTML</a></li>
        <li><a href="app.php?function=6&nossoNumero=00031285570000000068">Baixar</a></li>
    </ul>
    
</body>
</html>