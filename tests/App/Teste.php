<?php

namespace Lumiun\CobrancasBB\Test\App;

use DateInterval;
use DateTimeZone;
use DateTimeImmutable;
use Lumiun\CobrancasBB\ChargeHandler;
use Lumiun\CobrancasBB\Classes\Boleto;
use Lumiun\CobrancasBB\Classes\Pagador;
use Lumiun\CobrancasBB\Classes\Beneficiario;
use Lumiun\CobrancasBB\Classes\BeneficiarioFinal;

class Teste
{
    // Dados de autenticação na API -> Class: Handler
    private $state = 'HM';
    private $gwDevAppKey = '';
    private $clientId = '';
    private $clientSecret = '';

    // Dados do beneficiário -> Class: Beneficiario
    private $numeroConvenio = 3128557;
    private $numeroCarteira = 17;
    private $numeroVariacaoCarteira = 35;
    private $agenciaBeneficiario = '452-9';
    private $contaBeneficiario = '123873-5';
    private $razaoSocialBeneficiario = 'Yago e Débora Limpeza ME';
    private $numeroInscricaoBeneficiario = '81.402.887/0001-46';
    private $enderecoBeneficiario = 'Rua Francisco de Balboa, 176';
    private $bairroBeneficiario = 'São Vicente';
    private $cidadeBeneficiario = 'Gravataí';
    private $ufBeneficiario = 'RS';
    private $cepBeneficiario = '94155-060';

    private $beneficiario;
    private $handler;

    public function __construct()
    {
        $this->gwDevAppKey .= 'd27bd7790fffab801367e17d60050156b9e1a5bd';

        $this->clientId .= 'eyJpZCI6IiIsImNvZGlnb1B1YmxpY2Fkb3IiOjAsImNvZGlnb1NvZnR3YXJlIjoxNDMwMiwic2VxdWVuY2lhbEluc3';
        $this->clientId .= 'RhbGFjYW8iOjF9';

        $this->clientSecret .= 'eyJpZCI6IjdiN2E5YTMtODliMS00MzI3LWJjZmQtYWE0YWRhNzYzIiwiY29kaWdvUHVibGljYWRvciI6MCwiY2';
        $this->clientSecret .= '9kaWdvU29mdHdhcmUiOjE0MzAyLCJzZXF1ZW5jaWFsSW5zdGFsYWNhbyI6MSwic2VxdWVuY2lhbENyZWRlbmNp';
        $this->clientSecret .= 'YWwiOjEsImFtYmllbnRlIjoiaG9tb2xvZ2FjYW8iLCJpYXQiOjE2MTY3MDMzODE4OTh9';

        $this->beneficiario = new Beneficiario(
            $this->numeroConvenio,
            $this->numeroCarteira,
            $this->numeroVariacaoCarteira,
            $this->agenciaBeneficiario,
            $this->contaBeneficiario,
            $this->razaoSocialBeneficiario,
            $this->numeroInscricaoBeneficiario,
            $this->enderecoBeneficiario,
            $this->bairroBeneficiario,
            $this->cidadeBeneficiario,
            $this->ufBeneficiario,
            $this->cepBeneficiario
        );
        $this->handler = new ChargeHandler(
            $this->beneficiario,
            $this->gwDevAppKey,
            $this->clientId,
            $this->clientSecret,
            $this->state
        );
    }

    public function registerJson()
    {
        // Dados do pagador -> Pagador
        $pagadorPayload = [
            'numeroInscricao' => '96050176876',
            'nome' => 'Alerio de Aguiar Zorzato',
            'endereco' => 'Rua Osvaldo Rieck, 79',
            'cep' => '98700000',
            'cidade' => 'Ijuí',
            'bairro' => 'Morada do Sol',
            'uf' => 'RS',
            'telefone' => '55991234567',
        ];
        /** Recebe os dados do pagador: CPF/CNPJ, nome, endereço e etc... */
        $pagador = new Pagador($pagadorPayload);

        // Dados do avalista -> BeneficiarioFinal
        $avalistaPayload = [
            'numeroInscricao' => '960.501.768-76',
            'nome' => 'Alerio de Aguiar Zorzato',
        ];
        /** Recebe os dados do avalista: CPF/CNPJ e nome */
        $avalista = new BeneficiarioFinal($avalistaPayload);

        // Dados da Cobrança -> Boleto
        $now = new DateTimeImmutable('now', new DateTimeZone('America/Sao_Paulo'));
        $plusOneWeek = $now->add(new DateInterval('P1W'));
        $boletoPayload = [
            'dataVencimento' => $plusOneWeek->format('Y-m-d'),
            'valorOriginal' => '89,56',
            'numeroTituloBeneficiario' => rand(99999999, 9999999999),
            'campoUtilizacaoBeneficiario' => 'TESTE',
            'mensagemBloquetoOcorrencia' => 'Pagamento de teste.',
            'multa' => [
                'porcentagem' => 2
            ],
            'jurosMora' => [
                'porcentagem' => 1
            ],
            'indicadorPix' => 'S',
        ];
        /** Recebe os dados da cobrança*/
        $boleto = new Boleto($boletoPayload);

        /** Registra um Boleto
         *  Recebe:
         *  $pagador Pagador
         *  $boleto Boleto
         *  $avalista BeneficiarioFinal -> Opcional.
         */
        $charge = $this->handler->register($pagador, $boleto, $avalista);

        return $charge;
    }

    public function registerHtml()
    {
        // Dados do pagador -> Pagador
        $pagadorPayload = [
            'numeroInscricao' => '96050176876',
            'nome' => 'Alerio de Aguiar Zorzato',
            'endereco' => 'Rua Osvaldo Rieck, 79',
            'cep' => '98700000',
            'cidade' => 'Ijuí',
            'bairro' => 'Morada do Sol',
            'uf' => 'RS',
            'telefone' => '55991234567',
        ];
        /** Recebe os dados do pagador: CPF/CNPJ, nome, endereço e etc... */
        $pagador = new Pagador($pagadorPayload);

        // Dados do avalista -> BeneficiarioFinal
        $avalistaPayload = [
            'numeroInscricao' => '960.501.768-76',
            'nome' => 'Alerio de Aguiar Zorzato',
        ];
        /** Recebe os dados do avalista: CPF/CNPJ e nome */
        $avalista = new BeneficiarioFinal($avalistaPayload);

        // Dados da Cobrança -> Boleto
        $now = new DateTimeImmutable('now', new DateTimeZone('America/Sao_Paulo'));
        $plusOneWeek = $now->add(new DateInterval('P1W'));
        $boletoPayload = [
            'dataVencimento' => $plusOneWeek->format('Y-m-d'),
            'valorOriginal' => '89,56',
            'numeroTituloBeneficiario' => rand(99999999, 9999999999),
            'campoUtilizacaoBeneficiario' => 'TESTE',
            'mensagemBloquetoOcorrencia' => 'Pagamento de teste.',
            'multa' => [
                'porcentagem' => 2
            ],
            'jurosMora' => [
                'porcentagem' => 1
            ],
            'indicadorPix' => 'S',
        ];
        /** Recebe os dados da cobrança*/
        $boleto = new Boleto($boletoPayload);

        /** Registra um Boleto
         *  Recebe:
         *  $pagador Pagador
         *  $boleto Boleto
         *  $avalista BeneficiarioFinal -> Opcional.
         */
        $charge = $this->handler->register($pagador, $boleto, $avalista, false);

        return $charge;
    }

    public function registerMany()
    {
        $payload = $this->getPayload();

        echo "<br>Iniciando registro de boletos <br>";
        $start = new DateTimeImmutable('now', new DateTimeZone('America/Sao_Paulo'));
        echo $start->format('d/m/Y - H:i:s');
        foreach ($payload as $key => $charge) {
            $charge = $this->handler->register($charge['pagador'], $charge['boleto']);

            echo '<pre>';
            echo $key . "<br>";
            var_dump($charge['json']);
            echo '</pre>';
            sleep(20);
        }
        echo "<br>Finalizando registro de boletos <br>";
        $end = new DateTimeImmutable('now', new DateTimeZone('America/Sao_Paulo'));
        echo $end->format('d/m/Y - H:i:s');
    }

    public function getList()
    {
        $now = new DateTimeImmutable('now', new DateTimeZone('America/Sao_Paulo'));
        $plusOneWeek = $now->add(new DateInterval('P1W'));

        $chargesList = $this->handler->get([
            /*
            * Passado filtro padrão pois em homologação a API tem muitos
            * registros e fica instável com consultas muito amplas.
            */
            'dataInicioVencimento' => $plusOneWeek->format('Y-m-d'),
            'dataFimVencimento' => $plusOneWeek->format('Y-m-d'),
            'dataInicioRegistro' => $now->format('Y-m-d'),
            'dataFimRegistro' => $now->format('Y-m-d'),
        ]);

        return $chargesList;
    }

    public function detail($id, $json = false)
    {
        /** Número do Título
         *  Gerado ao registrar um boleto.
         */
        $numeroTituloCliente = $id;

        /**
         * Retorna os detalhes da cobrança
         * Recebe: numeroTituloCliente -> "nosso numero" do Boleto.
         */
        $boleto = $this->handler->get($numeroTituloCliente, $json);

        /* Exibe erros retornados do pacote BbCharge */
        if (isset($boleto['errors'])) {
            foreach (array_unique($boleto['errors']) as $error) {
                if (is_array($error)) {
                    echo "{$error['code']}: {$error['message']}" . '<br>';
                    die;
                }
                echo "$error" . '<br>';
            }
            die;
        }

        return $boleto;
    }

    public function destroy($id)
    {
        /** Número do Título
         *  Gerado ao registrar um boleto.
         */
        $numeroTituloCliente = $id;

        /** Da baixa em um Boleto
         *  Recebe: numeroTituloCliente -> "nosso numero" do Boleto.
         */
        $destroy = $this->handler->destroy($numeroTituloCliente);

        return $destroy;
    }

    public function update()
    {
        // code...
    }

    public function getPayload()
    {
        $payload = [];

        for ($i = 0; $i < 10; $i++) {
            // Dados do pagador -> Pagador
            $pagadorPayload = [
                'numeroInscricao' => '96050176876',
                'nome' => 'Alerio de Aguiar Zorzato',
                'endereco' => 'Rua Osvaldo Rieck, 79',
                'cep' => '98700000',
                'cidade' => 'Ijuí',
                'bairro' => 'Morada do Sol',
                'uf' => 'RS',
                'telefone' => '55991234567',
            ];
            /** Recebe os dados do pagador: CPF/CNPJ, nome, endereço e etc... */
            $pagador = new Pagador($pagadorPayload);

            // Dados da Cobrança -> Boleto
            $now = new DateTimeImmutable('now', new DateTimeZone('America/Sao_Paulo'));
            $plusOneWeek = $now->add(new DateInterval('P1W'));
            $boletoPayload = [
                'dataVencimento' => $plusOneWeek->format('Y-m-d'),
                'valorOriginal' => '89,56',
                'numeroTituloBeneficiario' => rand(99999999, 9999999999),
                'campoUtilizacaoBeneficiario' => 'TESTE',
                'mensagemBloquetoOcorrencia' => 'Pagamento de teste.',
                'multa' => [
                    'porcentagem' => 2
                ],
                'jurosMora' => [
                    'porcentagem' => 1
                ],
                'indicadorPix' => 'S',
            ];
            /** Recebe os dados da cobrança*/
            $boleto = new Boleto($boletoPayload);

            $payload[] = ['boleto' => $boleto, 'pagador' => $pagador];
        }

        return $payload;
    }
}
