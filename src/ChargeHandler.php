<?php

/*
   Copyright 2021 Lumiun Tecnologia LTDA - ME

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

namespace Lumiun\CobrancasBB;

use GuzzleHttp\Client;
use Lumiun\CobrancasBB\Classes\Boleto;
use Lumiun\CobrancasBB\Classes\Pagador;
use GuzzleHttp\Exception\RequestException;
use Lumiun\CobrancasBB\Middleware\ApiAuth;
use Lumiun\CobrancasBB\Classes\Beneficiario;
use Lumiun\CobrancasBB\Classes\BoletoRender;
use Lumiun\CobrancasBB\Middleware\GetParameters;
use Lumiun\CobrancasBB\Classes\BeneficiarioFinal;

class ChargeHandler
{
    /**
     * API authentication class.
     *
     * @var ApiAuth
     */
    private $auth;
    /**
     * Payee class
     * Equivalent to API Beneficiario.
     *
     * @var Beneficiario
     */
    private $beneficiario;

    /**
     * Defines the API environment
     * Domain: 'HM' - 'HOMOLOGACAO', 'PROD' - 'PRODUCAO'.
     *
     * @var string
     */
    private $environment;

    private $apiBaseUri;

    /**
     * Creates a new billing handler.
     * It has the public methods:
     * - register: Creates a new charge;
     * - get: Query and list billings;
     * - terminate: Write off a charge;
     * - update: Update a charge.
     *
     * @param Beneficiario $beneficiario
     * @param string $gwDevAppKey
     * @param string $clientId
     * @param string $clientSecret
     * @param string $environment
     */
    public function __construct($beneficiario, $gwDevAppKey, $clientId, $clientSecret, $environment = null)
    {
        if ($environment === 'PROD' || $environment === 'PRODUCAO') {
            $this->apiBaseUri = 'https://api.bb.com.br/cobrancas/v2';
            $this->environment = 'PROD';
        } else {
            $this->apiBaseUri = 'https://api.hm.bb.com.br/cobrancas/v2';
            $this->environment = 'HM';
        }

        $this->auth = new ApiAuth($environment, $gwDevAppKey, $clientId, $clientSecret);
        $this->beneficiario = $beneficiario;
    }

    /**
     * Register a new charge.
     *
     * On success returns an array with billing data.
     * If failure returns an array with the errors.
     *
     * @param Pagador           $payer     Dados do Pagador
     * @param Boleto            $charge    Dados do Boleto
     * @param BeneficiarioFinal $guarantee Opcional: Dados do Avalista
     *
     * @return array
     */
    public function register($payer, $charge, $guarantee = null, $html = false)
    {
        $charge->setBeneficiario($this->beneficiario);
        $payload = [];
        $temp = [];

        $temp['beneficiario'] = $this->beneficiario->toArray();
        $temp['cobranca'] = $charge->toArray();
        $temp['pagador'] = $payer->toArray();
        $temp['beneficiarioFinal'] = $guarantee ? $guarantee->toArray() : null;

        foreach ($temp as $value) {
            if (isset($value['errors'])) {
                $payload['errors'] = $value['errors'];
            }
        }

        if (isset($payload['errors'])) {
            $payload['message'] = 'Erro - ' . __CLASS__ . ' - ' . __FUNCTION__ . ' - Erro na validação dos dados (Pacote de cobranças).';
            $payload['statusCode'] = 400;
            return $payload;
        }

        $payload = array_merge($payload, $temp['beneficiario'], $temp['cobranca']);
        $payload['pagador'] = $temp['pagador'];
        $guarantee ? $payload['beneficiarioFinal'] = $temp['beneficiarioFinal'] : null;

        $payload = json_encode($payload);

        $tries = 0;

        $response = $this->registerRequest($payload);

        while (
            (int) $response['statusCode'] !== 201
            && $tries < 3
        ) {
            $this->auth->clearCache();
            $response = $this->registerRequest($payload);
            $tries++;
        }

        if ($tries === 3 && isset($response['errors']) && (int) $response['statusCode'] !== 201) {
            $return = $response;
            // Do a switch case to handle the errors according to the status code.
            switch ((int) $response['statusCode']) {
                case 401:
                    $return['message'] = 'Erro no Token. Não foi possivel autenticar na API, entre em contato com o suporte.';
                    break;

                // 500 - Internal server error or 503 - Service unavailable
                case 500:
                case 503:
                    $return['message'] = 'Ocorreu um erro interno na API, tente novamente mais tarde.';
                    
                    break;
                
                default:
                    $return['message'] = 'Ocorreu um erro ao registrar a cobrança.';

                    break;
            }

            return $return;            
        }

        $qrCode = $response['qrCode'] ?? null;

        if (isset($response['numero'])) {
            $detailRequest = $this->detail($response['numero'], $html, $qrCode);

            while (
                (int) $detailRequest['statusCode'] !== 200
                && $tries < 3
            ) {
                $this->auth->clearCache();
                $detailRequest = $this->detail($response['numero'], $html, $qrCode);
                $tries++;
            }

            $response['detailsStatusCode'] = $detailRequest['statusCode'];

            if ($tries === 3 && isset($detailRequest['errors']) && (int) $detailRequest['statusCode'] !== 200) {
                $return = $response;
                switch ((int) $detailRequest['statusCode']) {
                    // 401 - Unauthorized
                    case 401:
                        $return['message'] = 'Erro no Token. Não foi possivel autenticar na API, para capturar os detalhes da cobrança, entre em contato com o suporte.';

                        break;
    
                    // 500 - Internal server error
                    case 500:
                    // 503 - Service unavailable
                    case 503:
                        $return['message'] = 'Ocorreu um erro interno na API ao capturar os detalhes da cobrança, tente novamente mais tarde.';
                        
                        break;
                    
                    // 404 - Not found
                    case 404:
                        $return['message'] = 'Cobrança não encontrada para o número informado. Não foi possivel capturar os detalhes da cobrança.';

                        break;
                    
                    default:
                        $return['message'] = 'Ocorreu um erro ao capturar os detalhes da cobrança.';
    
                        break;
                }
    
                return $return;            
            }

            if ($html) {
                $response['html'] = $detailRequest['charge'];
            } else {
                $response['json'] = $detailRequest['charge'];
            }
        }

        return $response;
    }

    /**
     * Makes the request to the API billing registration facility and returns an array with the billing data.
     *
     * @param string $payload
     *
     * @return array $boleto
     */
    public function registerRequest(string $payload)
    {
        $guzzleHeader = $this->auth->getHeader();

        if (is_array($guzzleHeader) and isset($guzzleHeader['errors'])) {
            return ['errors' => ['Erro - ' . __CLASS__ . ' - ' . __FUNCTION__ . ' - ' . json_encode($guzzleHeader)], 'statusCode' => '500'];
        }

        try {
            $guzzle = new Client($guzzleHeader);

            $response = $guzzle->request(
                'POST',
                $this->apiBaseUri . '/boletos'
                    . '?gw-dev-app-key='
                    . $this->auth->getAppKey(),
                [
                    'body' => $payload,
                ]
            );

            $body = $response->getBody();

            $contents = $body->getContents();

            $boleto = json_decode($contents, true);

            if (!$boleto) {
                return [
                    'errors' => [
                        'Erro - ' . __CLASS__ . ' - ' . __FUNCTION__ . ' - Impossivel converter o retorno da API.',
                    ],
                    'statusCode' => $response->getStatusCode() ? $response->getStatusCode() : '500',
                ];
            }

            $boleto['statusCode'] = $response->getStatusCode();

            return $boleto;
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $contents = $response->getBody()->getContents();
                if ($e->getCode() === 401) {
                    return [
                        'errors' => [
                            'Erro - ' . $e->getCode() . ' - ' . __CLASS__ . ' - ' . __FUNCTION__ . ' - ' . $contents,
                        ],
                        'statusCode' => $e->getCode(),
                    ];
                }

                return [
                    'errors' => [
                        'Erro - ' . $e->getCode() . ' - ' . __CLASS__ . ' - ' . __FUNCTION__ . ' - ' . $contents,
                    ],
                    'statusCode' => $e->getCode(),
                ];
            }

            return [
                'errors' => [
                    'Erro - ' . __CLASS__ . ' - ' . __FUNCTION__ . ' - A requisicao nao obteve resposta da API.',
                ],
                'statusCode' => '500'
            ];
        }
    }

    public function get($data = null, $html = false)
    {

        if (!is_array($data) && !is_null($data)) {
            return $this->detail($data, $html);
        }

        $data['beneficiario'] = $this->beneficiario->toArray();

        $query = new GetParameters($data);
        $queryParams = $query->queryParams();

        if (isset($queryParams['errors'])) {
            return $queryParams;
        }

        $guzzleHeader = $this->auth->getHeader();

        if (is_array($guzzleHeader) and isset($guzzleHeader['errors'])) {
            return ['errors' => ['Erro - ' . __CLASS__ . ' - ' . __FUNCTION__ . ' - ' . json_encode($guzzleHeader)]];
        }

        try {
            $guzzle = new Client($guzzleHeader);

            /* Requisição */
            $response = $guzzle->request(
                'GET',
                $this->apiBaseUri . '/boletos?gw-dev-app-key='
                    . $this->auth->getAppKey()
                    . $queryParams
            );

            /* Recuperar o corpo da resposta da requisição */
            $body = $response->getBody();

            /* Acessar os dados da resposta - JSON */
            $contents = $body->getContents();

            /* Converter o JSON em um array associativo PHP */
            $boletos = json_decode($contents, true);

            return $boletos;
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $contents = $response->getBody()->getContents();

                return [
                    'errors' => [
                        'Erro - ' . $e->getCode() . ' - ' . __CLASS__ . ' - ' . __FUNCTION__ . ' - ' . $contents,
                    ]
                ];
            }

            return [
                'errors' => [
                    'Erro - ' . __CLASS__ . ' - ' . __FUNCTION__ . ' - A requisicao nao obteve resposta da API.',
                ],
            ];
        }
    }

    public function detail($id, $html, $qrCode = null)
    {
        /**
         * Cria um Boleto para validar o numeroTituloCliente recebido
         * e posteriormente renderizar o retorno da API no layout do boleto.
         */
        $charge = new BoletoRender([
            'numeroTituloCliente' => $id,
            'beneficiario' => $this->beneficiario,
        ]);

        $numeroBoletoBB = $charge->getNumeroTituloCliente();

        if (isset($numeroBoletoBB['errors'])) {
            return ['errors' => ['Erro - ' . __CLASS__ . ' - ' . __FUNCTION__ . ' - ' . json_encode($numeroBoletoBB)], 'statusCode' => '400'];
        }

        $guzzleHeader = $this->auth->getHeader();

        if (is_array($guzzleHeader) and isset($guzzleHeader['errors'])) {
            return ['errors' => ['Erro - ' . __CLASS__ . ' - ' . __FUNCTION__ . ' - ' . json_encode($guzzleHeader)], 'statusCode' => '500'];
        }

        try {
            $guzzle = new Client($guzzleHeader);

            /* Requisição */
            $response = $guzzle->request(
                'GET',
                $this->apiBaseUri . '/boletos'
                    . '/' . $numeroBoletoBB
                    . '?gw-dev-app-key=' . $this->auth->getAppKey()
                    . '&numeroConvenio=' . $this->beneficiario->getNumeroConvenio()
            );

            $body = $response->getBody();

            $contents = $body->getContents();

            $detail = json_decode($contents, true);

            $detail['qrCode'] = $qrCode;

            $charge->setData($detail);

            $return = [
                'statusCode' => $response->getStatusCode(),
            ];

            if ($html) {
                $return['charge'] = $charge->HTML();
            } else {
                $return['charge'] = $charge->json();
            }

            return $return;
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $contents = $response->getBody()->getContents();

                return [
                    'errors' => [
                        'Erro - ' . $e->getCode() . ' - ' . __CLASS__ . ' - ' . __FUNCTION__ . ' - ' . $contents,
                    ],
                    'statusCode' => $e->getCode(),
                ];
            }

            return [
                'errors' => [
                    'Erro - ' . __CLASS__ . ' - ' . __FUNCTION__ . ' - A requisicao nao obteve resposta da API.',
                ],
                'statusCode' => $e->getCode(),
            ];
        }
    }

    public function destroy($numeroTituloCliente)
    {
        /** Cria um Boleto para validar o numeroTituloCliente recebido */
        $charge = new Boleto([
            'numeroTituloCliente' => $numeroTituloCliente,
            'numeroConvenio' => $this->beneficiario->getNumeroConvenio(),
        ]);

        $numero = $charge->getNumeroTituloCliente();

        if (isset($numero['errors'])) {
            return $numero;
        }

        $guzzleHeader = $this->auth->getHeader();

        if (is_array($guzzleHeader) and isset($guzzleHeader['errors'])) {
            return [
                'errors' => [
                    'Erro - ' . __CLASS__ . ' - ' . __FUNCTION__ . ' - ' . json_encode($guzzleHeader),
                ],
            ];
        }

        try {
            $guzzle = new Client($guzzleHeader);

            /* Requisição */
            $response = $guzzle->request(
                'POST',
                $this->apiBaseUri . '/boletos'
                    . '/' . $numero
                    . '/baixar'
                    . '?gw-dev-app-key='
                    . $this->auth->getAppKey(),
                [
                    'body' => json_encode([
                        'numeroConvenio' => $this->beneficiario->getNumeroConvenio(),
                    ]),
                ]
            );

            $body = $response->getBody();

            $contents = $body->getContents();

            $baixa = json_decode($contents, true);

            return $baixa;
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $contents = $response->getBody()->getContents();

                if ($e->getCode() === 400) {
                    $result = json_decode($contents, true);
                    if (isset($result['errors']) and is_array($result['errors'])) {
                        $errorsMessage = [];
                        foreach ($result['errors'] as $error) {
                            $errorsMessage['errors'][] = $error['message'];
                        }

                        return $errorsMessage;
                    }
                }

                return [
                    'errors' => [
                        'Erro - ' . $e->getCode() . ' - ' . __CLASS__ . ' - ' . __FUNCTION__ . ' - ' . $contents,
                    ],
                ];
            }

            return [
                'errors' => [
                    'Erro - ' . __CLASS__ . ' - ' . __FUNCTION__ . ' - A requisicao nao obteve resposta da API.',
                ],
            ];
        }
    }
}
