<?php

/*
   Copyright 2021 Lumiun Tecnologia LTDA - ME

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

namespace Lumiun\CobrancasBB\Middleware;

use DateTimeImmutable;

class GetParameters
{
    use Validation;

    /**
     * Define a faixa de boletos a ser pesquisada. Sempre em MAIÚSCULA.
     *
     * Domínio: A - boletos em ser; B - boletos baixados, liquidados ou protestados.
     *
     * @var string
     */
    private $indicadorSituacao = 'A';

    /**
     * @var int
     */
    private $agenciaBeneficiario;

    /**
     * @var int
     */
    private $contaBeneficiario;

    /**
     * @var int
     */
    private $carteiraConvenio;

    /**
     * @var int
     */
    private $variacaoCarteiraConvenio;

    /**
     * Indica a modalidade de cobrança na qual o boleto está cadastrado no BB.
     *
     * Domínio: 1 - Simples; 4 - Vinculada.
     *
     * @var int
     */
    private $modalidadeCobranca;

    /**
     * CNPJ do pagador a ser pesquisado, sem os dígitos verificadores.
     *
     * @var int|string
     */
    private $cnpjPagador;

    /**
     * Dígito do CNPJ do pagador a ser pesquisado, sem zeros a esquerda.
     *
     * @var int|string
     */
    private $digitoCNPJPagador;

    /**
     * CPF do pagador a ser pesquisado, sem os dígitos verificadores.
     *
     * @var int|string
     */
    private $cpfPagador;

    /**
     * Dígito do CPF do pagador a ser pesquisado, sem zeros a esquerda.
     *
     * @var int|string
     */
    private $digitoCPFPagador;

    /**
     * Data inicial de vencimento que delimita o período da consulta, formato dd.mm.aaaa.
     *
     * @var DateTimeImmutable
     */
    private $dataInicioVencimento;

    /**
     * Data final de vencimento que delimita o período da consulta, formato dd.mm.aaaa.
     *
     * @var DateTimeImmutable
     */
    private $dataFimVencimento;

    /**
     * Data inicial de registro que delimita o período da consulta, formato dd.mm.aaaa.
     *
     * @var DateTimeImmutable
     */
    private $dataInicioRegistro;

    /**
     * Data fim de registro que delimita o período da consulta, formato dd.mm.aaaa.
     *
     * @var DateTimeImmutable
     */
    private $dataFimRegistro;

    /**
     * Data início de movimento que delimita o período de consulta de boletos baixados, liquidados ou protestados,
     * formato dd.mm.aaaa.
     *
     * @var DateTimeImmutable
     */
    private $dataInicioMovimento;

    /**
     * Data fim de movimento que delimita o período de consulta de boletos baixados, liquidados ou protestados,
     * formato dd.mm.aaaa.
     *
     * @var DateTimeImmutable
     */
    private $dataFimMovimento;

    /**
     * Código da situação atual do boleto.
     * Domínios:
     * 1 - NORMAL
     * 2 - MOVIMENTO CARTORIO
     * 3 - EM CARTORIO
     * 4 - TITULO COM OCORRENCIA DE CARTORIO
     * 5 - PROTESTADO ELETRONICO
     * 6 - LIQUIDADO
     * 7 - BAIXADO
     * 8 - TITULO COM PENDENCIA DE CARTORIO
     * 9 - TITULO PROTESTADO MANUAL
     * 10 - TITULO BAIXADO/PAGO EM CARTORIO
     * 11 - TITULO LIQUIDADO/PROTESTADO
     * 12 - TITULO LIQUID/PGCRTO
     * 13 - TITULO PROTESTADO AGUARDANDO BAIXA
     * 14 - TITULO EM LIQUIDACAO
     * 15 - TITULO AGENDADO BB
     * 16 - TITULO CREDITADO
     * 17 - PAGO EM CHEQUE - AGUARD.LIQUIDACAO
     * 18 - PAGO PARCIALMENTE
     * 19 - PAGO PARCIALMENTE CREDITADO
     * 21 - TITULO AGENDADO OUTROS BANCOS.
     *
     * @var int
     */
    private $codigoEstadoTituloCobranca;

    /**
     * Define se a pesquisa trará apenas boletos vencidos ou não. Sempre MAIÚSCULA.
     *
     * Domínio: S - Sim; N - Não.
     *
     * @var string
     */
    private $boletoVencido;

    /**
     * Representa o índice da listagem pelo qual sua pesquisa se iniciará, podendo retornar
     * até 300 registros por chamada.
     *
     * @var int
     */
    private $indice;

    /**
     * @var \Lumiun\CobrancasBB\Classes\Beneficiario
     */
    private $beneficiario;

    /**
     * @var string
     */
    private $numeroInscricao;
    private $tipoInscricao;

    public function __construct($data)
    {
        foreach ($data as $key => $value) {
            if (!property_exists(self::class, $key)) {
                continue;
            }
            switch ($key) {
                case 'beneficiario':
                    $this->agenciaBeneficiario = $value['agenciaBeneficiario'];
                    $this->contaBeneficiario = $value['contaBeneficiario'];
                    $this->carteiraConvenio = $value['numeroCarteira'];
                    $this->variacaoCarteiraConvenio = $value['numeroVariacaoCarteira'];

                    break;

                case 'numeroInscricao':
                    if ($value) {
                        $this->numeroInscricao = $this->cleanDocument($value);
                        $this->tipoInscricao = $this->documentType($this->numeroInscricao);
                        $this->setDocument();
                    }

                    break;

                case 'boletoVencido':
                    if (mb_strtolower($value) === 'on') {
                        $this->boletoVencido = 'S';
                    } else {
                        $this->boletoVencido = $value;
                    }

                    break;

                case strpos($key, 'data') === 0:
                    if ($value) {
                        $this->{$key} = new DateTimeImmutable($value);
                    }

                    break;

                default:
                    $this->{$key} = $value;

                    break;
            }
        }
    }

    /**
     * Get domínio: A - boletos em ser; B - boletos baixados, liquidados ou protestados.
     *
     * @return string
     */
    public function getIndicadorSituacao()
    {
        $domain = ['A', 'B'];

        $this->domain($domain, 'indicadorSituacao', 'indicador da situação');

        return $this->string($this->indicadorSituacao);
    }

    /**
     * Get the value of agenciaBeneficiario.
     *
     * @return int
     */
    public function getAgenciaBeneficiario()
    {
        return $this->int($this->agenciaBeneficiario);
    }

    /**
     * Get the value of contaBeneficiario.
     *
     * @return int
     */
    public function getContaBeneficiario()
    {
        return $this->int($this->contaBeneficiario);
    }

    /**
     * Get the value of carteiraConvenio.
     *
     * @return int
     */
    public function getCarteiraConvenio()
    {
        return $this->int($this->carteiraConvenio);
    }

    /**
     * Get the value of variacaoCarteiraConvenio.
     *
     * @return int
     */
    public function getVariacaoCarteiraConvenio()
    {
        return $this->int($this->variacaoCarteiraConvenio);
    }

    /**
     * Get domínio: 1 - Simples; 4 - Vinculada.
     *
     * @return int
     */
    public function getModalidadeCobranca()
    {
        if ($this->modalidadeCobranca) {
            $domain = [1, 4];

            $this->domain($domain, 'modalidadeCobranca', 'modalidade da cobrança');
        }

        return $this->int($this->modalidadeCobranca);
    }

    /**
     * Get cNPJ do pagador a ser pesquisado, sem os dígitos verificadores.
     *
     * @return int
     */
    public function getCnpjPagador()
    {
        return $this->int($this->cnpjPagador);
    }

    /**
     * Get dígito do CNPJ do pagador a ser pesquisado, sem zeros a esquerda.
     *
     * @return int
     */
    public function getDigitoCNPJPagador()
    {
        return $this->int($this->digitoCNPJPagador);
    }

    /**
     * Get cPF do pagador a ser pesquisado, sem os dígitos verificadores.
     *
     * @return int
     */
    public function getCpfPagador()
    {
        return $this->int($this->cpfPagador);
    }

    /**
     * Get dígito do CPF do pagador a ser pesquisado, sem zeros a esquerda.
     *
     * @return int
     */
    public function getDigitoCPFPagador()
    {
        return $this->int($this->digitoCPFPagador);
    }

    /**
     * Get data inicial de vencimento que delimita o período da consulta, formato dd.mm.aaaa.
     *
     * @return string
     */
    public function getDataInicioVencimento()
    {
        return $this->date($this->dataInicioVencimento);
    }

    /**
     * Get data final de vencimento que delimita o período da consulta, formato dd.mm.aaaa.
     *
     * @return string
     */
    public function getDataFimVencimento()
    {
        return $this->date($this->dataFimVencimento);
    }

    /**
     * Get data inicial de registro que delimita o período da consulta, formato dd.mm.aaaa.
     *
     * @return string
     */
    public function getDataInicioRegistro()
    {
        return $this->date($this->dataInicioRegistro);
    }

    /**
     * Get data fim de registro que delimita o período da consulta, formato dd.mm.aaaa.
     *
     * @return string
     */
    public function getDataFimRegistro()
    {
        return $this->date($this->dataFimRegistro);
    }

    /**
     * Get data início de movimento que delimita o período de consulta de boletos baixados, liquidados ou protestados,
     * formato dd.mm.aaaa.
     *
     * @return string
     */
    public function getDataInicioMovimento()
    {
        return $this->date($this->dataInicioMovimento);
    }

    /**
     * Get data fim de movimento que delimita o período de consulta de boletos baixados, liquidados ou protestados,
     * formato dd.mm.aaaa.
     *
     * @return string
     */
    public function getDataFimMovimento()
    {
        return $this->date($this->dataFimMovimento);
    }

    /**
     * Get 21 - TITULO AGENDADO OUTROS BANCOS.
     *
     * @return int
     */
    public function getCodigoEstadoTituloCobranca()
    {
        $domain = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 21];

        if ($this->codigoEstadoTituloCobranca) {
            $this->domain($domain, 'codigoEstadoTituloCobranca', 'código do estado da cobrança');
        }

        return $this->int($this->codigoEstadoTituloCobranca);
    }

    /**
     * Get domínio: S - Sim; N - Não.
     *
     * @return string
     */
    public function getBoletoVencido()
    {
        $domain = ['S', 'N'];

        if ($this->boletoVencido) {
            $this->domain($domain, 'boletoVencido', 'mostrar somente boletos vencidos');
        }

        return $this->string($this->boletoVencido);
    }

    /**
     * Get representa o índice da listagem pelo qual sua pesquisa se iniciará,
     * podendo retornar até 300 registros por chamada.
     *
     * @return int
     */
    public function getIndice()
    {
        return $this->int($this->indice);
    }

    /**
     * @return string|array
     */
    public function queryParams()
    {
        $query = '';

        foreach (get_class_methods($this) as $method) {
            if (strpos($method, 'get') !== false) {
                $value = $this->{$method}();

                if (is_numeric($value)) {
                    if ($value > 0) {
                        $query .= '&' . lcfirst(str_replace(['get'], ['', ''], $method)) . '=' . $value;
                    }
                } else {
                    if (!empty($value) || strlen(trim($value)) > 0) {
                        $query .= '&' . lcfirst(str_replace(['get'], ['', ''], $method)) . '=' . $value;
                    }
                }
            }
        }

        if (isset($this->errors)) {
            return ['errors' => array_unique($this->errors)];
        }

        return $query;
    }

    private function setDocument()
    {
        $this->documentValidation($this->tipoInscricao, $this->numeroInscricao);

        if ($this->tipoInscricao === 1) {
            $this->cpfPagador = substr($this->numeroInscricao, 0, 9);
            $this->digitoCPFPagador = substr($this->numeroInscricao, -2);
        } else {
            $this->cnpjPagador = substr($this->numeroInscricao, 0, 12);
            $this->digitoCNPJPagador = substr($this->numeroInscricao, -2);
        }
    }
}
