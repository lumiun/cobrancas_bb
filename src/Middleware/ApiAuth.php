<?php

/*
   Copyright 2021 Lumiun Tecnologia LTDA - ME

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

namespace Lumiun\CobrancasBB\Middleware;

use DateInterval;
use DateTimeImmutable;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class ApiAuth
{
    /**
     * Developer Key.
     *
     * @var string */
    private $gwDevAppKey;
    /**
     * Client Basic (Base64).
     *
     * @var string */
    private $authorization;
    /**
     * Client Id.
     *
     * @var string */
    private $clientId;
    /**
     * Client Secret.
     *
     * @var string */
    private $clientSecret;

    /**
     * Defines the oAuth API URI.
     *
     * @var string
     */
    private $oAuthUri;

    private $cacheDir;
    private $cacheFile;

    public function __construct($environment, $gwDevAppKey, $clientId, $clientSecret)
    {
        if ($environment === 'PROD') {
            $this->oAuthUri = 'https://oauth.bb.com.br/oauth/token';
        } else {
            $this->oAuthUri = 'https://oauth.hm.bb.com.br/oauth/token';
        }

        $this->cacheDir = mb_substr(__DIR__, 0, strripos(__DIR__, '/') + 1) . 'Storage/cache';
        $this->cacheFile = $this->cacheDir . '/token.json';

        $this->gwDevAppKey = $gwDevAppKey;
        $this->clientId = $clientId;
        $this->clientSecret = $clientSecret;
        $this->authorization = 'Basic ' . base64_encode($this->clientId . ':' . $this->clientSecret);
    }

    /**
     * Generate a New Authentication Token.
     *
     * Needs to be tested
     *
     * @return mixed
     */
    private function generateAccessToken()
    {
        try {
            $guzzle = new Client([
                'headers' => [
                    'gw-dev-app-key' => $this->gwDevAppKey,
                    'Authorization' => $this->authorization,
                    'Content-Type' => 'application/x-www-form-urlencoded',
                ],
                'verify' => false,
            ]);

            $response = $guzzle->request(
                'POST',
                $this->oAuthUri . '?gw_dev_app_key=' . $this->gwDevAppKey,
                [
                    'form_params' => [
                        'grant_type' => 'client_credentials',
                        'client_id' => $this->clientId,
                        'client_secret' => $this->clientSecret,
                        'scope' => 'cobrancas.boletos-info cobrancas.boletos-requisicao',
                    ],
                ]
            );

            $body = $response->getBody();

            $contents = $body->getContents();

            $token = json_decode($contents);

            $token->expires_in = $this->getExpirationTimestamp($token->expires_in - 50);

            return $token;
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $contents = $response->getBody()->getContents();

                return [
                    'errors' => [
                        'Erro - ' . $e->getCode() . ' - ' . __CLASS__ . ' - ' . __FUNCTION__ . ' - ' . $contents,
                    ],
                ];
            }

            return [
                'errors' => [
                    'Erro - ' . __CLASS__ . ' - ' . __FUNCTION__ . ' - A requisicao nao obteve resposta da API.',
                ]
            ];
        }
    }

    /**
     * Get the string value of the API Access Token.
     *
     * @return string|array
     */
    public function getAccessToken()
    {
        if (file_exists($this->cacheFile)) {
            $cachedFile = file_get_contents($this->cacheFile);

            $token = json_decode($cachedFile);

            if (is_object($token)) {
                if ($this->checkIfTokenIsValid($token->expires_in)) {
                    return $token->access_token;
                }
            }
        }

        $result = $this->generateAccessToken();

        if (!is_object($result) and isset($result['errors'])) {
            return $result;
        }

        $cachedFile = file_put_contents($this->cacheFile, json_encode($result));

        return $result->access_token;
    }

    /**
     * Get the headers for requests to the API.
     *
     * @return array
     */
    public function getHeader()
    {

        $token = $this->getAccessToken();

        if (is_array($token) and isset($token['errors'])) {
            return $token;
        }

        $header = [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
                'Content-Type' => 'application/json',
            ],
            'verify' => false,
        ];

        return $header;
    }

    /**
     * Get the developer key, needed to make requests to the API.
     *
     * @return string
     */
    public function getAppKey()
    {
        return $this->gwDevAppKey;
    }

    /**
     * Receives token TTL in seconds. From the current moment on, it returns the timestamp in
     * which the token will expire.
     *
     * Needs to be tested
     *
     * @param int $seconds
     *
     * @return int
     */
    private function getExpirationTimestamp($seconds)
    {
        $now = new DateTimeImmutable();

        return $now->add(new DateInterval('PT' . $seconds . 'S'))->getTimestamp();
    }

    /**
     * Checks if the cached token is still valid.
     *
     * @param $timestamp
     *
     * @return bool
     */
    public function checkIfTokenIsValid($timestamp)
    {
        $now = new DateTimeImmutable();
        $nowTimestamp = $now->getTimestamp();

        return $timestamp >= $nowTimestamp;
    }

    /**
     * Clears cached files
     *
     * @return bool
     */
    public function clearCache()
    {
        if (file_exists($this->cacheFile)) {
            if (unlink($this->cacheFile)) {
                return true;
            }
        }

        return false;
    }
}
