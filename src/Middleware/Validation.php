<?php

/*
   Copyright 2021 Lumiun Tecnologia LTDA - ME

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

namespace Lumiun\CobrancasBB\Middleware;

trait Validation
{
    protected $errors;

    protected function documentValidation(int $tipoInscricao, $document)
    {
        if ($tipoInscricao === 1) {
            return $this->validateCpf($document);
        } elseif ($tipoInscricao === 2) {
            return $this->validateCnpj($document);
        }

        return false;
    }

    protected function domain($domain, $attribute, $fieldName = null, $message = null)
    {
        $fieldName = $fieldName ?: $attribute;
        $message = $message ?: "O campo $fieldName nao corresponde ao dominio especificado pela API.";

        if (!in_array($this->{$attribute}, $domain)) {
            $this->errors[] = $message;

            return false;
        }

        return true;
    }

    protected function minLength($length, $attribute, $fieldName = null)
    {
        $fieldName = $fieldName ?: $attribute;
        if (mb_strlen($this->{$attribute}) < $length) {
            $this->errors[] = "O campo $fieldName deve ter no minimo $length caracteres.";

            return false;
        }

        return true;
    }

    protected function maxLength($length, $attribute, $fieldName = null)
    {
        $fieldName = $fieldName ?: $attribute;
        if (mb_strlen($this->{$attribute}) > $length) {
            $this->errors[] = "O campo $fieldName nao pode ter mais de $length caracteres.";

            return false;
        }

        return true;
    }

    protected function exactLength($length, $attribute, $fieldName = null)
    {
        $fieldName = $fieldName ?: $attribute;

        if (mb_strlen($this->{$attribute}) !== $length) {
            $this->errors[] = "O campo $fieldName deve ter exatamente $length caracteres.";

            return false;
        }

        return true;
    }

    protected function validateCpf($cpf)
    {
        if (preg_match('/(\d)\1{10}/', $cpf)) {
            $this->errors[] = 'O CPF nao e valido.';

            return false;
        }

        for ($t = 9; $t < 11; ++$t) {
            for ($d = 0, $c = 0; $c < $t; ++$c) {
                $d += $cpf[$c] * (($t + 1) - $c);
            }
            $d = ((10 * $d) % 11) % 10;
            if ($cpf[$c] != $d) {
                $this->errors[] = 'O CPF nao e valido.';

                return false;
            }
        }

        return true;
    }

    protected function validateCnpj($cnpj)
    {
        if (preg_match('/(\d)\1{13}/', $cnpj)) {
            $this->errors[] = 'O CNPJ nao e valido.';

            return false;
        }

        $b = [6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2];

        for ($i = 0, $n = 0; $i < 12; $n += $cnpj[$i] * $b[++$i]);

        if ($cnpj[12] != ((($n %= 11) < 2) ? 0 : 11 - $n)) {
            $this->errors[] = 'O CNPJ nao e valido.';

            return false;
        }

        for ($i = 0, $n = 0; $i <= 12; $n += $cnpj[$i] * $b[$i++]);

        if ($cnpj[13] != ((($n %= 11) < 2) ? 0 : 11 - $n)) {
            $this->errors[] = 'O CNPJ nao e valido.';

            return false;
        }

        return true;
    }

    protected function className()
    {
        $class = substr(strrchr(get_class($this), '\\'), 1);

        return lcfirst($class);
    }

    protected function onlyNumber($attribute, $fieldName = null)
    {
        $fieldName = $fieldName ?: $attribute;

        $value = $this->cleanDocument($this->{$attribute});

        if (!preg_match('/^[0-9]*$/', $value)) {
            $this->errors[] = 'O campo ' . $fieldName . ' deve conter apenas numeros.';

            return false;
        }

        return true;
    }

    protected function required($attribute, $fieldName = null)
    {
        $fieldName = $fieldName ?: $attribute;
        $value = $this->{$attribute};

        if (is_numeric($value)) {
            if (!isset($value)) {
                $this->errors[] = 'O campo ' . $fieldName . ' e obrigatorio, por favor verifique!';

                return false;
            }
        } elseif (is_object($value)) {
            if (!isset($value)) {
                $this->errors[] = 'O campo ' . $fieldName . ' e obrigatorio, por favor verifique!';

                return false;
            }
        } else {
            if (empty($value) || mb_strlen(trim($value)) === 0) {
                $this->errors[] = 'O campo ' . $fieldName . ' e obrigatorio, por favor verifique!';

                return false;
            }
        }

        return true;
    }

    protected function string($value)
    {
        $value = str_replace('                         ', '', $value);

        return filter_var($value, FILTER_SANITIZE_STRING);
    }

    protected function int($value)
    {
        return filter_var(ltrim($value, '0'), FILTER_VALIDATE_INT);
    }

    protected function float($value)
    {
        $value = preg_replace('/[^0-9\.\,\s]/', '', str_replace(' ', '', $value));
        if (!filter_var($value, FILTER_VALIDATE_FLOAT)) {
            $value = str_replace('.', '', $value);
            $value = str_replace(',', '.', $value);
        }

        if (!is_numeric($value)) {
            return false;
        }

        $value += 0;

        $value = number_format($value, 2, '.', '');

        return filter_var($value, FILTER_VALIDATE_FLOAT);
    }

    protected function documentType($document)
    {
        if (mb_strlen($document) === 11) {
            return 1;
        } elseif (mb_strlen($document) === 14) {
            return 2;
        }

        $this->errors[] = 'O CPF/CNPJ nao e valido';

        return false;
    }

    protected function cleanDocument($value)
    {
        $value = preg_replace('/[^a-zA-Z0-9\s]/', '', str_replace(' ', '', $value));

        return $value;
    }

    protected function removeDigit($value)
    {
        if (strpos($value, '-')) {
            $value = substr($value, 0, strpos($value, '-'));
        }

        return $value;
    }

    protected function date($date)
    {
        if (isset($date)) {
            return $date->format('d.m.Y');
        }
    }
}
