<?php

/*
   Copyright 2021 Lumiun Tecnologia LTDA - ME

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

namespace Lumiun\CobrancasBB\Classes;

use DateInterval;
use DateTimeImmutable;
use JsonSerializable;
use Lumiun\CobrancasBB\Middleware\Validation;

class Multa implements JsonSerializable
{
    use Validation;

    private $tipo = 2;
    private $data;
    private $porcentagem = 2.00;
    private $valor;

    public function __construct($data)
    {
        if (is_array($data)) {
            if (count($data) !== 0) {
                foreach ($data as $key => $value) {
                    if (!property_exists(self::class, $key)) {
                        continue;
                    }
                    switch ($key) {
                        case 'data':
                            $this->data = new DateTimeImmutable($value);
                            break;
                        default:
                            $this->{$key} = $value;
                            break;
                    }
                }
            }
        } else {
            if (!$data) {
                return;
            }
            $this->data = $data->add(new DateInterval('P1D'));
        }
    }

    /**
     * Get type of discount.
     * 0 - Sem desconto; 1 - Valor fixo até a data informada; 2 - percentual até a data informada.
     *
     * @return int
     */
    public function getTipo()
    {
        $domain = [0, 1, 2];

        $this->domain($domain, 'tipo', 'tipo do desconto');

        return intval($this->tipo);
    }

    /**
     * Get the value of data.
     *
     * @return string
     */
    public function getData()
    {
        return $this->date($this->data);
    }

    /**
     * Get the value of porcentagem.
     *
     * @return float|void
     */
    public function getPorcentagem()
    {
        if ($this->tipo === 2) {
            return $this->float($this->porcentagem);
        }

        return;
    }

    /**
     * Get the value of valor.
     *
     * @return float|void
     */
    public function getValor()
    {
        if ($this->tipo === 1) {
            $this->required('valor', 'valor da multa, quando o tipo é 1,');

            return $this->float($this->valor);
        }

        return;
    }

    /**
     * Get the discount data.
     *
     * @return array
     */
    public function toArray()
    {
        $arr = [];

        $arr['tipo'] = intval($this->getTipo());

        foreach (get_class_methods($this) as $method) {
            if (strpos($method, 'get') !== false) {
                $value = $this->{$method}();

                if (is_numeric($value)) {
                    if ($value > 0) {
                        $arr[lcfirst(str_replace(['get'], ['', ''], $method))] = $value;
                    }
                } else {
                    if (!empty($value) || strlen(trim($value)) > 0) {
                        $arr[lcfirst(str_replace(['get'], ['', ''], $method))] = $value;
                    }
                }
            }
        }

        if (isset($this->errors)) {
            return ['errors' => array_unique($this->errors)];
        }

        return $arr;
    }

    public function setData($tipo, $porcentagem = null, $valor = null)
    {
        $this->tipo = $tipo;
        if ($tipo === 1 || $tipo === 2) {
            if ($tipo === 1) {
                $this->valor = $valor;
            } else {
                $this->porcentagem = $porcentagem;
            }
        }
    }

    public function formatedData()
    {
        $data = [];

        $data['tipo'] = $this->getTipo();
        if ($this->tipo === 1) {
            $data['multa'] = 'R$ ' . number_format($this->getValor(), 2, ',', '.');
        }
        if ($this->tipo === 2) {
            $data['multa'] = number_format($this->getPorcentagem(), 2, ',', '.') . '%';
        }

        return $data;
    }

    public function jsonSerialize()
    {
        return $this->toArray();
    }
}
