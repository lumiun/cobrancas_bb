<?php

/*
   Copyright 2021 Lumiun Tecnologia LTDA - ME

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

namespace Lumiun\CobrancasBB\Classes;

use JsonSerializable;
use Lumiun\CobrancasBB\Middleware\Validation;

class JurosMora implements JsonSerializable
{
    use Validation;

    private $tipo = 2;
    private $porcentagem = 1.00;
    private $valor;

    public function __construct($data)
    {
        if (count($data) !== 0) {
            foreach ($data as $key => $value) {
                if (!property_exists(self::class, $key)) {
                    continue;
                }

                $this->{$key} = $value;
            }
        }
    }

    /**
     * Get type of discount.
     * 0 - Dispensar; 1 - Valor fixo por dia de atraso; 2 - Taxa mensal; 3 - Isento.
     *
     * @return int
     */
    public function getTipo()
    {
        $domain = [0, 1, 2, 3];

        $this->domain($domain, 'tipo', 'tipo de juros');

        return intval($this->tipo);
    }

    /**
     * Get the value of porcentagem.
     *
     * @return float|void
     */
    public function getPorcentagem()
    {
        if ($this->tipo === 2) {
            if (!$this->required('porcentagem', 'porcentagem do desconto, quando o tipo é 2,')) {
                return;
            }

            return $this->float($this->porcentagem);
        }

        return;
    }

    /**
     * Get the value of valor.
     *
     * @return float|void
     */
    public function getValor()
    {
        if ($this->tipo === 1) {
            if (!$this->required('valor', 'valor do desconto, quando o tipo é 1,')) {
                return;
            }

            return $this->float($this->valor);
        }

        return;
    }

    /**
     * Get the discount data.
     *
     * @return array
     */
    public function toArray()
    {
        $arr = [];

        $arr['tipo'] = intval($this->getTipo());

        foreach (get_class_methods($this) as $method) {
            if (strpos($method, 'get') !== false) {
                $value = $this->{$method}();

                if (is_numeric($value)) {
                    if ($value > 0) {
                        $arr[lcfirst(str_replace(['get'], ['', ''], $method))] = $value;
                    }
                } else {
                    if (!empty($value) || strlen(trim($value)) > 0) {
                        $arr[lcfirst(str_replace(['get'], ['', ''], $method))] = $value;
                    }
                }
            }
        }

        if (isset($this->errors)) {
            return ['errors' => array_unique($this->errors)];
        }

        return $arr;
    }

    public function setData($tipo, $porcentagem = null, $valor = null)
    {
        $this->tipo = $tipo;
        if ($tipo === 1) {
            $this->valor = $valor;
        }
        if ($tipo === 2) {
            $this->porcentagem = $porcentagem;
        }
    }

    public function formatedData()
    {
        $data = [];

        $data['tipo'] = $this->getTipo();
        if ($this->tipo === 1) {
            $data['juros'] = 'R$ ' . number_format($this->getValor(), 2, ',', '.');
        }
        if ($this->tipo === 2) {
            $data['juros'] = number_format(($this->getPorcentagem() / 30), 3, ',', '.') . '%';
        }
        if ($this->tipo === 3) {
            $data['juros'] = 'cobrança isenta de juros';
        }

        return $data;
    }

    public function jsonSerialize()
    {
        return $this->toArray();
    }
}
