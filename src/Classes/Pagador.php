<?php

/*
   Copyright 2021 Lumiun Tecnologia LTDA - ME

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

namespace Lumiun\CobrancasBB\Classes;

use JsonSerializable;
use Lumiun\CobrancasBB\Middleware\Validation;

class Pagador implements JsonSerializable
{
    use Validation;

    /**
     * Type of registration document.
     * Domain: 1 - CPF | 2 - CNPJ.
     *
     * @var int
     */
    private $tipoInscricao;
    /**
     * Number of registration document.
     *
     * @var int|string
     */
    private $numeroInscricao;
    /**
     * Payer name.
     *
     * @var string
     */
    private $nome;
    /**
     * @var string
     */
    private $endereco;
    /**
     * @var int|string
     */
    private $cep;
    /**
     * @var string
     */
    private $cidade;
    /**
     * @var string
     */
    private $bairro;
    /**
     * @var string
     */
    private $uf;
    /**
     * @var string
     */
    private $telefone;

    public function __construct($data)
    {
        foreach ($data as $key => $value) {
            if (!property_exists(self::class, $key)) {
                continue;
            }
            switch ($key) {
                case 'numeroInscricao':
                    $this->numeroInscricao = $this->cleanDocument($value);
                    $this->tipoInscricao = $this->documentType($this->numeroInscricao);
                    break;
                case 'cep':
                    $this->cep = $this->cleanDocument($value);
                    break;
                case 'telefone':
                    $this->telefone = $this->cleanDocument($value);
                    break;
                case 'uf':
                    $this->uf = strtoupper($value);
                    break;
                default:
                    $this->{$key} = $value;
                    break;
            }
        }
    }

    /**
     * Get type of registration document.
     *
     * @return int
     */
    public function getTipoInscricao()
    {
        return $this->tipoInscricao;
    }

    /**
     * Get number of registration document.
     *
     * @return int
     */
    public function getNumeroInscricao()
    {
        $this->required('numeroInscricao', 'CPF/CNPJ');
        $this->documentValidation($this->tipoInscricao, $this->numeroInscricao);

        return $this->int($this->numeroInscricao);
    }

    /**
     * Get payer name.
     *
     * @return string
     */
    public function getNome()
    {
        $this->required('nome');
        $this->minLength(5, 'nome');
        $this->maxLength(30, 'nome');

        return $this->string($this->nome);
    }

    /**
     * Get the value of endereco.
     *
     * @return string
     */
    public function getEndereco()
    {
        $this->required('endereco');
        $this->minLength(3, 'endereco');
        $this->maxLength(30, 'endereco');

        return $this->string($this->endereco);
    }

    /**
     * Get the value of cep.
     *
     * @return int
     */
    public function getCep()
    {
        $this->required('cep', 'CEP');
        $this->onlyNumber('cep', 'CEP');
        $this->exactLength(8, 'cep', 'CEP');

        return $this->int($this->cep);
    }

    /**
     * Get the value of cidade.
     *
     * @return string
     */
    public function getCidade()
    {
        $this->required('cidade');
        $this->minLength(3, 'cidade');
        $this->maxLength(20, 'cidade');

        return $this->string($this->cidade);
    }

    /**
     * Get the value of bairro.
     *
     * @return string
     */
    public function getBairro()
    {
        $this->required('bairro');
        $this->maxLength(30, 'bairro');

        return $this->string($this->bairro);
    }

    /**
     * Get the value of uf.
     *
     * @return string
     */
    public function getUf()
    {
        $domain = [
            'AC',
            'AL',
            'AM',
            'AP',
            'BA',
            'CE',
            'DF',
            'ES',
            'GO',
            'MA',
            'MG',
            'MS',
            'MT',
            'PA',
            'PB',
            'PE',
            'PI',
            'PR',
            'RJ',
            'RN',
            'RO',
            'RR',
            'RS',
            'SC',
            'SE',
            'SP',
            'TO'
        ];
        $this->required('uf', 'UF');
        $this->domain($domain, 'uf', 'UF', 'A UF informada não representa nenhum estado Brasileiro.');

        return $this->uf;
    }

    /**
     * Get the value of telefone.
     *
     * @return string
     */
    public function getTelefone()
    {
        $this->maxLength(30, 'telefone');

        return $this->string($this->telefone);
    }

    /**
     * Get the payer data.
     *
     * @return array
     */
    public function toArray()
    {
        $arr = [];

        foreach (get_class_methods($this) as $method) {
            if (strpos($method, 'get') !== false) {
                $value = $this->{$method}();

                if (is_numeric($value)) {
                    if ($value > 0) {
                        $arr[lcfirst(str_replace(['get'], ['', ''], $method))] = $value;
                    }
                } else {
                    if (!empty($value) || strlen(trim($value)) > 0) {
                        $arr[lcfirst(str_replace(['get'], ['', ''], $method))] = $value;
                    }
                }
            }
        }

        if (isset($this->errors)) {
            return ['errors' => array_unique($this->errors)];
        }

        return $arr;
    }

    public function setData($tipoInscricao, $numeroInscricao, $nome, $endereco, $cep, $cidade, $bairro, $uf)
    {
        $this->tipoInscricao = $tipoInscricao;
        if ($tipoInscricao === 1) {
            $this->numeroInscricao = str_pad($numeroInscricao, 11, '0', STR_PAD_LEFT);
        } else {
            $this->numeroInscricao = str_pad($numeroInscricao, 14, '0', STR_PAD_LEFT);
        }
        $this->nome = $nome;
        $this->endereco = $endereco;
        $this->cep = str_pad($cep, 8, '0', STR_PAD_LEFT);
        $this->cidade = $cidade;
        $this->bairro = $bairro;
        $this->uf = $uf;
    }

    public function formatedData()
    {
        $data = [];

        $data['numeroInscricao'] = $this->formatDocument($this->numeroInscricao);
        $data['nome'] = $this->nome;
        $data['endereco'] = $this->endereco;
        $data['cep'] = $this->formatDocument($this->cep);
        $data['cidade'] = $this->cidade;
        $data['bairro'] = $this->bairro;
        $data['uf'] = $this->uf;

        return $data;
    }

    /**
     * Format numeroInscricao.
     *
     * @return string|bool
     */
    public function formatDocument($value)
    {
        switch (strlen($value)) {
            case 8:
                // Format ##.###-###
                $result = substr($value, 0, 2) . '.';
                $result .= substr($value, 2, 3) . '-';
                $result .= substr($value, 5, 3) . '';

                return $result;
            case 11:
                // Format ###.###.###-##
                $result = substr($value, 0, 3) . '.';
                $result .= substr($value, 3, 3) . '.';
                $result .= substr($value, 6, 3) . '-';
                $result .= substr($value, 9, 2) . '';

                return $result;
            case 14:
                // Format ##.###.###/####-##
                $result = substr($value, 0, 2) . '.';
                $result .= substr($value, 2, 3) . '.';
                $result .= substr($value, 5, 3) . '/';
                $result .= substr($value, 8, 4) . '-';
                $result .= substr($value, 12, 2) . '';

                return $result;
            default:
                return false;
        }
    }

    public function jsonSerialize()
    {
        return $this->toArray();
    }
}
