<?php

/*
   Copyright 2021 Lumiun Tecnologia LTDA - ME

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

namespace Lumiun\CobrancasBB\Classes;

use DateTimeImmutable;

class BoletoRender extends Boleto
{
    private $html;

    private $resourcesDir;

    /**
     * Corresponding HTML field "field-cod-banco".
     *
     * @var string
     */
    private $codigoBanco;

    /**
     * Corresponding HTML field "linha-digitavel".
     *
     * @var string
     */
    private $linhaDigitavel;

    /**
     * Corresponding HTML fields:
     *                     "rp-contrato-cobr"
     *                     "fc-contrato-cobr".
     *
     * @var string
     */
    private $numeroContratoCobranca;

    /**
     * Corresponding HTML field: "Data process.".
     *
     * @var DateTimeImmutable
     */
    private $dataRegistro;

    /**
     * Corresponding HTML field: "Código de Barras".
     *
     * @var string
     */
    private $textoCodigoBarrasTituloCobranca;

    /**
     * Data used to generate the PIX QRCode.
     *
     * @var array
     */
    private $pixQrCodeData;

    /**
     * Corresponding HTML fields:
     *                     "rp-pagador-nome"
     *                     "rp-pagador-documento"
     *                     "rp-pagador-endereco"
     *                     "fc-pagador".
     *
     * @var Pagador
     */
    private $pagador;

    /**
     * If true, add the default logos to Json
     *
     * @var bool
     */
    private $logos = false;

    /**
     * Stores information about billing status updates.
     *
     * @var array
     */
    private $informacoesEstadoCobranca;

        /**
         *
         *
         * @var string
         */
        private $codigoEstadoCobranca;

        /**
         *
         * @var DateTimeImmutable
         */
        private $dataLimiteRecebimento;

        /**
         *
         * @var DateTimeImmutable
         */
        private $dataBaixaAutomatica;

        /**
         *
         * @var string
         */
        private $codigoTipoLiquidacao;

        /**
         *
         * @var string
         */
        private $codigoTipoBaixaTitulo;

        /**
         *
         * @var float
         */
        private $valorPagoSacado;

        /**
         *
         * @var float
         */
        private $valorAbatimento;

        /**
         *
         * @var float
         */
        private $valorDesconto;

        /**
         *
         * @var float
         */
        private $valorJuroMora;

        /**
         *
         * @var float
         */
        private $valorMulta;

        /**
         *
         * @var float
         */
        private $valorReajuste;

        /**
         *
         * @var float
         */
        private $valorOutro;

        /**
         *
         * @var float
         */
        private $valorCreditoCedente;

        /**
         *
         * @var DateTimeImmutable
         */
        private $dataRecebimentoTitulo;

        /**
         *
         * @var DateTimeImmutable
         */
        private $dataCreditoLiquidacao;

    public function __construct($data)
    {
        parent::__construct($data);
        $this->pagador = new Pagador([]);
        $this->resourcesDir = mb_substr(__DIR__, 0, strripos(__DIR__, '/') + 1) . 'Resources/';
        if (isset($data['logos'])) {
            $this->logos = $data['logos'];
        }
    }

    public function setData(array $data)
    {
        $this->pagador->setData(
            $data['codigoTipoInscricaoSacado'],
            $data['numeroInscricaoSacadoCobranca'],
            $data['nomeSacadoCobranca'],
            $data['textoEnderecoSacadoCobranca'],
            $data['numeroCepSacadoCobranca'],
            $data['nomeMunicipioSacadoCobranca'],
            $data['nomeBairroSacadoCobranca'],
            $data['siglaUnidadeFederacaoSacadoCobranca']
        );

        $this->multa->setData(
            $data['codigoTipoMulta'],
            $data['percentualMultaTitulo'],
            $data['valorMultaTituloCobranca']
        );

        $this->jurosMora->setData(
            $data['codigoTipoJuroMora'],
            $data['percentualJuroMoraTitulo'],
            $data['valorJuroMoraTitulo']
        );

        foreach ($data as $key => $value) {
            switch ($key) {
                case 'codigoLinhaDigitavel':
                    $this->linhaDigitavel = $value;
                    $this->codigoBanco = substr($value, 0, 4);
                    break;
                case 'numeroTituloCedenteCobranca':
                    $this->numeroTituloBeneficiario = $value;
                    break;
                case 'numeroContratoCobranca':
                    $this->numeroContratoCobranca = $value;
                    break;
                case 'dataVencimentoTituloCobranca':
                    $this->dataVencimento = new DateTimeImmutable($value);
                    break;
                case 'dataEmissaoTituloCobranca':
                    $this->dataEmissao = new DateTimeImmutable($value);
                    break;
                case 'dataRegistroTituloCobranca':
                    $this->dataRegistro = new DateTimeImmutable($value);
                    break;
                case 'valorOriginalTituloCobranca':
                    $this->valorOriginal = $value;
                    break;
                case 'codigoTipoTituloCobranca':
                    $this->codigoTipoTitulo = $value;
                    $this->descricaoTipoTitulo = $this->setDescricaoTipoTitulo($this->codigoTipoTitulo);
                    break;
                case 'codigoAceiteTituloCobranca':
                    $this->codigoAceite = $value;
                    break;
                case 'textoCampoUtilizacaoCedente':
                    $this->campoUtilizacaoBeneficiario = $value;
                    break;
                case 'textoMensagemBloquetoTitulo':
                    $this->mensagemBloquetoOcorrencia = $value;
                    break;
                case 'textoCodigoBarrasTituloCobranca':
                    $this->textoCodigoBarrasTituloCobranca = $value;
                    break;
                case 'beneficiario':
                    $this->beneficiario = $value;
                    break;
                case 'qrCode':
                    $this->pixQrCodeData = $value;
                    break;
                case 'codigoEstadoTituloCobranca':
                    $this->codigoEstadoCobranca = $this->setEstadoTituloCobrancaByCodigo($value);
                    break;
                case 'dataLimiteRecebimentoTitulo':
                    $this->dataLimiteRecebimento =
                        ($value !== "") && ($value !== "01.01.0001") ? new DateTimeImmutable($value) : null;
                    break;
                case 'dataBaixaAutomaticoTitulo':
                    $this->dataBaixaAutomatica =
                        ($value !== "") && ($value !== "01.01.0001") ? new DateTimeImmutable($value) : null;
                    break;
                case 'codigoTipoLiquidacao':
                    $this->codigoTipoLiquidacao = $this->setTipoLiquidacaoByCodigo($value);
                    break;
                case 'codigoTipoBaixaTitulo':
                    $this->codigoTipoBaixaTitulo = $this->setTipoBaixaByCodigo($value);
                    break;
                case 'valorPagoSacado':
                    $this->valorPagoSacado = $this->float($value);
                    break;
                case 'valorAbatimentoTotal':
                    $this->valorAbatimento = $this->float($value);
                    break;
                case 'valorDescontoUtilizado':
                    $this->valorDesconto = $this->float($value);
                    break;
                case 'valorJuroMoraRecebido':
                    $this->valorJuroMora = $this->float($value);
                    break;
                case 'valorMultaRecebido':
                    $this->valorMulta = $this->float($value);
                    break;
                case 'valorReajuste':
                    $this->valorReajuste = $this->float($value);
                    break;
                case 'valorOutroRecebido':
                    $this->valorOutro = $this->float($value);
                    break;
                case 'valorCreditoCedente':
                    $this->valorCreditoCedente = $this->float($value);
                    break;
                case 'dataRecebimentoTitulo':
                    $this->dataRecebimentoTitulo =
                        ($value !== "") && ($value !== "01.01.0001") ? new DateTimeImmutable($value) : null;
                    break;
                case 'dataCreditoLiquidacao':
                    $this->dataCreditoLiquidacao =
                        ($value !== "") && ($value !== "01.01.0001") ? new DateTimeImmutable($value) : null;
                    break;
                default:
                    break;
            }
        }

        $this->informacoesEstadoCobranca = [
            'codigoEstadoCobranca' => $this->codigoEstadoCobranca,
            'dataLimiteRecebimento' =>
                $this->dataLimiteRecebimento !== null
                ? $this->dataLimiteRecebimento->format('d/m/Y')
                : $this->dataLimiteRecebimento,
            'dataBaixaAutomatica' =>
                $this->dataBaixaAutomatica !== null
                ? $this->dataBaixaAutomatica->format('d/m/Y')
                : $this->dataBaixaAutomatica,
            'codigoTipoLiquidacao' => $this->codigoTipoLiquidacao,
            'codigoTipoBaixaTitulo' => $this->codigoTipoBaixaTitulo,
            'valorPagoSacado' => $this->valorPagoSacado,
            'valorAbatimento' => $this->valorAbatimento,
            'valorDesconto' => $this->valorDesconto,
            'valorJuroMora' => $this->valorJuroMora,
            'valorMulta' => $this->valorMulta,
            'valorReajuste' => $this->valorReajuste,
            'valorOutro' => $this->valorOutro,
            'valorCreditoCedente' => $this->valorCreditoCedente,
            'dataRecebimentoTitulo' =>
                $this->dataRecebimentoTitulo !== null
                ? $this->dataRecebimentoTitulo->format('d/m/Y')
                : $this->dataRecebimentoTitulo,
            'dataCreditoLiquidacao' =>
                $this->dataCreditoLiquidacao !== null
                ? $this->dataCreditoLiquidacao->format('d/m/Y')
                : $this->dataCreditoLiquidacao,
        ];
    }

    /**
     * Get the value of html.
     */
    public function HTML()
    {
        $this->logos = true;
        $this->setHtml();

        return $this->html;
    }

    /**
     * Get the value of pdf.
     */
    public function json()
    {
        $data = $this->formatData();

        $json = json_encode($data);

        return $json;
    }

    /**
     * Set the value of html.
     *
     * @return self
     */
    private function setHtml()
    {
        $data = $this->formatData();

        ob_start();

        extract($data);

        $css = file_get_contents($this->resourcesDir . 'css/CssCharge.css');

        //Generate barcode image
        $barcode = $this->generateBarcode($this->textoCodigoBarrasTituloCobranca);

        include $this->resourcesDir . 'html/HtmlCharge.php';

        $this->html = ob_get_clean();

        return $this;
    }

    private function formatData()
    {
        $data = [];

        if ($this->logos) {
            $data['logoBeneficiario'] = base64_encode(file_get_contents($this->resourcesDir . 'img/logoBenefic.png'));
            $data['logoBanco'] = base64_encode(file_get_contents($this->resourcesDir . 'img/logoBB.png'));
        }

        // Format: ###-#
        $data['codigoBanco'] = substr($this->codigoBanco, 0, 3) . '-' . substr($this->codigoBanco, 3, 1);

        // Format: #####.##### #####.###### #####.###### # ##############
        $data['linhaDigitavel'] = substr($this->linhaDigitavel, 0, 5) . '.';
        $data['linhaDigitavel'] .= substr($this->linhaDigitavel, 5, 5) . ' ';
        $data['linhaDigitavel'] .= substr($this->linhaDigitavel, 10, 5) . '.';
        $data['linhaDigitavel'] .= substr($this->linhaDigitavel, 15, 6) . ' ';
        $data['linhaDigitavel'] .= substr($this->linhaDigitavel, 21, 5) . '.';
        $data['linhaDigitavel'] .= substr($this->linhaDigitavel, 26, 6) . ' ';
        $data['linhaDigitavel'] .= substr($this->linhaDigitavel, 32, 1) . ' ';
        $data['linhaDigitavel'] .= substr($this->linhaDigitavel, 33, 14) . '';

        $data['barcodeNumber'] = $this->textoCodigoBarrasTituloCobranca;

        // Format: ###.###.###,##
        $data['valorOriginal'] = number_format($this->getValorOriginal(), 2, ',', '.');

        $data['pagador'] = $this->pagador->formatedData();
        $data['beneficiario'] = $this->beneficiario->formatedData();
        $data['jurosMora'] = $this->jurosMora->formatedData();

        $data['nossoNumero'] = $this->getNumeroTituloCliente();
        $data['numeroDocumento'] = $this->numeroTituloBeneficiario;
        $data['contratoCobranca'] = strval($this->numeroContratoCobranca);
        $data['dataVencimento'] = preg_replace('/\./', '/', $this->getDataVencimento());
        $data['dataEmissao'] = preg_replace('/\./', '/', $this->getDataEmissao());
        $data['dataRegistro'] = $this->dataRegistro->format('d/m/Y');
        $data['descricaoTipoTitulo'] = $this->descricaoTipoTitulo;
        $data['aceite'] = $this->getCodigoAceite();

        if ($this->getCampoUtilizacaoBeneficiario() === "") {
            $data['campoUtilizacaoBeneficiario'] = '<br>';
        } else {
            $data['campoUtilizacaoBeneficiario'] = $this->getCampoUtilizacaoBeneficiario();
        }
        $data['mensagemBloquetoOcorrencia'] = $this->getMensagemBloquetoOcorrencia();

        //Generate payment instructions
        $juros = $this->jurosMora->formatedData();
        $multa = $this->multa->formatedData();

        if ($multa['tipo'] !== 0 && $juros['tipo'] !== 0) {
            $data['instrucoesPagamento'] = '<br>';
            $data['instrucoesPagamento'] .= 'Após o vencimento, ';
            if ($multa['tipo'] === 1 || $multa['tipo'] === 2) {
                $data['instrucoesPagamento'] .= 'multa de ' . $multa['multa'];
            }
            if ($juros['tipo'] === 1 || $juros['tipo'] === 2) {
                if ($multa['tipo'] === 1 || $multa['tipo'] === 2) {
                    $data['instrucoesPagamento'] .= ' e ';
                }

                $data['instrucoesPagamento'] .= 'juros de ' . $juros['juros'] . ' ao dia.';
            }
        } else {
            $data['instrucoesPagamento'] = '&nbsp;';
        }

        $data['pix'] = $this->pixQrCodeData;

        $data['estadoAtualCobranca'] = $this->informacoesEstadoCobranca;

        $data['version'] = 2;

        return $data;
    }

    private function setDescricaoTipoTitulo($codigo)
    {
        $domain = [
            '1' => 'CH',
            '2' => 'DM',
            '3' => 'DMI',
            '4' => 'DS',
            '5' => 'DSI',
            '6' => 'DR',
            '7' => 'LC',
            '8' => 'NCC',
            '9' => 'NCE',
            '10' => 'NCI',
            '11' => 'NCR',
            '12' => 'NP',
            '13' => 'NPR',
            '14' => 'TM',
            '15' => 'TS',
            '16' => 'NS',
            '17' => 'RC',
            '18' => 'FAT',
            '19' => 'ND',
            '20' => 'AP',
            '21' => 'ME',
            '22' => 'PC',
            '23' => 'NF',
            '24' => 'DD',
            '25' => 'CPR',
            '26' => 'Warr.',
            '27' => 'DAE',
            '28' => 'DAM',
            '29' => 'DAU',
            '30' => 'Enc. Cond.',
            '31' => 'CC',
            '32' => 'BDP',
            '99' => 'Outros'
        ];

        return $domain[$codigo];
    }

    private function setEstadoTituloCobrancaByCodigo($codigo)
    {
        $domain = [
            '1' => '1 - NORMAL',
            '2' => '2 - MOVIMENTO CARTORIO',
            '3' => '3 - EM CARTORIO',
            '4' => '4 - TITULO COM OCORRENCIA DE CARTORIO',
            '5' => '5 - PROTESTADO ELETRONICO',
            '6' => '6 - LIQUIDADO',
            '7' => '7 - BAIXADO',
            '8' => '8 - TITULO COM PENDENCIA DE CARTORIO',
            '9' => '9 - TITULO PROTESTADO MANUAL',
            '10' => '10 - TITULO BAIXADO/PAGO EM CARTORIO',
            '11' => '11 - TITULO LIQUIDADO/PROTESTADO',
            '12' => '12 - TITULO LIQUID/PGCRTO',
            '13' => '13 - TITULO PROTESTADO AGUARDANDO BAIXA',
            '14' => '14 - TITULO EM LIQUIDACAO',
            '15' => '15 - TITULO AGENDADO',
            '16' => '16 - TITULO CREDITADO',
            '17' => '17 - PAGO EM CHEQUE AGUARD.LIQUIDACAO',
            '18' => '18 - PAGO PARCIALMENTE',
            '19' => '19 - PAGO PARCIALMENTE CREDITADO',
            '21' => '21 - TITULO AGENDADO COMPE',
            '80' => '80 - EM PROCESSAMENTO (ESTADO TRANSITÓRIO)',
        ];

        return $domain[$codigo];
    }

    private function setTipoLiquidacaoByCodigo($codigo)
    {
        $domain = [
            '0' => '0 - OUTROS',
            '1' => '1 - CAIXA',
            '2' => '2 - VIA COMPE',
            '3' => '3 - EM CARTORIO',
            '4' => '4 - EM CARTORIO - SEM EXISTENCIA 17 POS',
            '5' => '5 - TITULO EM LIQUIDACAO - ORIGEM AGE',
            '6' => '6 - TITULO EM LIQUIDACAO - PGT',
            '7' => '7 - BANCO POSTAL',
            '8' => '8 - TITULO LIQUIDADO VIA COMPE/STR',
            '9' => '9 - PIX',
        ];

        return $domain[$codigo];
    }

    private function setTipoBaixaByCodigo($codigo)
    {
        $domain = [
            '0' => '0 - OUTROS',
            '1' => '1 - BAIXADO POR SOLICITACAO',
            '2' => '2 - ENTREGA FRANCO PAGAMENTO',
            '9' => '9 - COMANDADA BANCO',
            '10' => '10 - COMANDADA CLIENTE - ARQUIVO',
            '11' => '11 - COMANDADA CLIENTE - ON-LINE',
            '12' => '12 - DECURSO PRAZO - CLIENTE',
            '13' => '13 - DECURSO PRAZO - BANCO',
            '15' => '15 - PROTESTADO',
            '31' => '31 - LIQUIDADO ANTERIORMENTE',
            '32' => '32 - HABILITADO EM PROCESSO',
            '35' => '35 - TRANSFERIDO PARA PERDAS',
            '51' => '51 - REGISTRADO INDEVIDAMENTE',
            '90' => '90 - BAIXA AUTOMATICA',
        ];

        return $domain[$codigo];
    }

    /**
     * Retorna a string contendo as imagens do código de barras, segundo o padrão Febraban.
     *
     * @return string
     */
    private function generateBarcode($code)
    {
        $barcodes = ['00110', '10001', '01001', '11000', '00101', '10100', '01100', '00011', '10010', '01010'];

        for ($f1 = 9; $f1 >= 0; --$f1) {
            for ($f2 = 9; $f2 >= 0; --$f2) {
                $f = ($f1 * 10) + $f2;
                $texto = '';

                for ($i = 1; $i < 6; ++$i) {
                    $texto .= substr($barcodes[$f1], ($i - 1), 1) . substr($barcodes[$f2], ($i - 1), 1);
                }

                $barcodes[$f] = $texto;
            }
        }

        // Guarda inicial
        $retorno = '<div class="barcode">' .
            '<div class="black thin"></div>' .
            '<div class="white thin"></div>' .
            '<div class="black thin"></div>' .
            '<div class="white thin"></div>';

        if (strlen($code) % 2 != 0) {
            $code = '0' . $code;
        }

        // Draw dos dados
        while (strlen($code) > 0) {
            $i = (int) round((float) self::caracteresEsquerda($code, 2));
            $code = self::caracteresDireita($code, strlen($code) - 2);
            $f = $barcodes[$i];

            for ($i = 1; $i < 11; $i += 2) {
                if (substr($f, ($i - 1), 1) == '0') {
                    $f1 = 'thin';
                } else {
                    $f1 = 'large';
                }

                $retorno .= "<div class='black {$f1}'></div>";

                if (substr($f, $i, 1) == '0') {
                    $f2 = 'thin';
                } else {
                    $f2 = 'large';
                }

                $retorno .= "<div class='white {$f2}'></div>";
            }
        }

        // Final
        return $retorno . '<div class="black large"></div>' .
            '<div class="white thin"></div>' .
            '<div class="black thin"></div>' .
            '</div>';
    }

    /**
     * Helper para obter os caracteres à esquerda.
     *
     * @param string $string
     * @param int    $num    Quantidade de caracteres para se obter
     *
     * @return string
     */
    private static function caracteresEsquerda($string, $num)
    {
        return substr($string, 0, $num);
    }

    /**
     * Helper para se obter os caracteres à direita.
     *
     * @param string $string
     * @param int    $num    Quantidade de caracteres para se obter
     *
     * @return string
     */
    private static function caracteresDireita($string, $num)
    {
        return substr($string, strlen($string) - $num, $num);
    }
}
