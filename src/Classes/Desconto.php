<?php

/*
   Copyright 2021 Lumiun Tecnologia LTDA - ME

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

namespace Lumiun\CobrancasBB\Classes;

use DateTimeImmutable;
use JsonSerializable;
use Lumiun\CobrancasBB\Middleware\Validation;

class Desconto implements JsonSerializable
{
    use Validation;

    /**
     * Type of discount.
     * 0 - Sem desconto; 1 - Valor fixo até a data informada; 2 - percentual até a data informada.
     *
     * @var int
     */
    private $tipo = 0;
    /**
     * Date of discount expiration.
     * It should only be set if the type is different of 0.
     *
     * @var string|DateTimeImmutable
     */
    private $dataExpiracao;
    /**
     * Discount percentage.
     * It should only be set if the type is equal to 2.
     *
     * @var string|float
     */
    private $porcentagem;
    /**
     * Discount value.
     * It should only be set if the type is equal to 1.
     *
     * @var string|float
     */
    private $valor;

    public function __construct($data)
    {
        if (count($data) !== 0) {
            foreach ($data as $key => $value) {
                if (!property_exists(self::class, $key)) {
                    continue;
                }
                switch ($key) {
                    case 'dataExpiracao':
                        $this->dataExpiracao = new DateTimeImmutable($value);
                        break;
                    default:
                        $this->{$key} = $value;
                        break;
                }
            }
        }
    }

    /**
     * Get type of discount.
     * 0 - Sem desconto; 1 - Valor fixo até a data informada; 2 - percentual até a data informada.
     *
     * @return int
     */
    public function getTipo()
    {
        $domain = [0, 1, 2];

        $this->domain($domain, 'tipo', 'tipo do desconto');

        return intval($this->tipo);
    }

    /**
     * Get the value of dataExpiracao.
     *
     * @return string|void
     */
    public function getDataExpiracao()
    {
        if ($this->tipo !== 0) {
            $this->required('dataExpiracao', 'data de expiração do desconto, quando o tipo é diferente de 0,');

            return $this->date($this->dataExpiracao);
        }

        return;
    }

    /**
     * Get the value of porcentagem.
     *
     * @return float|void
     */
    public function getPorcentagem()
    {
        if ($this->tipo === 2) {
            $this->required('porcentagem', 'porcentagem do desconto, quando o tipo é 2,');

            return $this->float($this->porcentagem);
        }

        return;
    }

    /**
     * Get the value of valor.
     *
     * @return float|void
     */
    public function getValor()
    {
        if ($this->tipo === 1) {
            $this->required('valor', 'valor do desconto, quando o tipo é 1,');

            return $this->float($this->valor);
        }

        return;
    }

    /**
     * Get the discount data.
     *
     * @return array
     */
    public function toArray()
    {
        $arr = [];

        $arr['tipo'] = intval($this->getTipo());

        foreach (get_class_methods($this) as $method) {
            if (strpos($method, 'get') !== false) {
                $value = $this->{$method}();

                if (is_numeric($value)) {
                    if ($value > 0) {
                        $arr[lcfirst(str_replace(['get'], ['', ''], $method))] = $value;
                    }
                } else {
                    if (!empty($value) || strlen(trim($value)) > 0) {
                        $arr[lcfirst(str_replace(['get'], ['', ''], $method))] = $value;
                    }
                }
            }
        }

        if (isset($this->errors)) {
            return ['errors' => array_unique($this->errors)];
        }

        return $arr;
    }

    public function jsonSerialize()
    {
        return $this->toArray();
    }
}
