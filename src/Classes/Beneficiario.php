<?php

/*
   Copyright 2021 Lumiun Tecnologia LTDA - ME

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

namespace Lumiun\CobrancasBB\Classes;

use Lumiun\CobrancasBB\Middleware\Validation;

class Beneficiario implements \JsonSerializable
{
    use Validation;

    /**
     * Number of the agreement with the bank.
     *
     * @var int
     */
    private $numeroConvenio;

    /**
     * Number of the billing portfolio.
     *
     * @var int
     */
    private $numeroCarteira;

    /**
     * Variation of the billing portfolio.
     *
     * @var int
     */
    private $numeroVariacaoCarteira;

    /**
     * Number of the bank agency.
     *
     * @var string
     */
    private $agenciaBeneficiario;

    /**
     * Number of the bank account.
     *
     * @var string
     */
    private $contaBeneficiario;

    /**
     * Company name.
     *
     * @var string
     */
    private $razaoSocial;

    /**
     * Type of registration document.
     * Domain: 1 - CPF | 2 - CNPJ.
     *
     * @var int
     */
    private $tipoInscricao;

    private $numeroInscricao;
    private $endereco;
    private $bairro;
    private $cidade;
    private $uf;
    private $cep;

    /**
     * Creates a new payee that represents the Beneficiário for the bank's API.
     *
     * @param int    $numeroConvenio
     * @param int    $numeroCarteira
     * @param int    $numeroVariacaoCarteira
     * @param string $agenciaBeneficiario
     * @param string $contaBeneficiario
     * @param string $razaoSocial
     * @param string $numeroInscricao
     * @param string $endereco
     * @param string $bairro
     * @param string $cidade
     * @param string $uf
     * @param string $cep
     */
    public function __construct(
        $numeroConvenio,
        $numeroCarteira,
        $numeroVariacaoCarteira,
        $agenciaBeneficiario,
        $contaBeneficiario,
        $razaoSocial,
        $numeroInscricao,
        $endereco,
        $bairro,
        $cidade,
        $uf,
        $cep
    ) {
        $this->numeroConvenio = $numeroConvenio;
        $this->numeroCarteira = $numeroCarteira;
        $this->numeroVariacaoCarteira = $numeroVariacaoCarteira;
        $this->agenciaBeneficiario = $agenciaBeneficiario;
        $this->contaBeneficiario = $contaBeneficiario;
        $this->razaoSocial = $razaoSocial;
        $this->numeroInscricao = $this->cleanDocument($numeroInscricao);
        $this->tipoInscricao = $this->documentType($this->numeroInscricao);
        $this->endereco = $endereco;
        $this->bairro = $bairro;
        $this->cidade = $cidade;
        $this->uf = $uf;
        $this->cep = $this->cleanDocument($cep);
    }

    /**
     * Get the number of the agreement.
     *
     * @return int
     */
    public function getNumeroConvenio()
    {
        $this->required('numeroConvenio');
        $this->onlyNumber('numeroConvenio');
        $this->exactLength(7, 'numeroConvenio');

        return $this->int($this->numeroConvenio);
    }

    /**
     * Get the number of the billing portfolio.
     *
     * @return int
     */
    public function getNumeroCarteira()
    {
        $this->required('numeroCarteira');
        $this->onlyNumber('numeroCarteira');

        return $this->int($this->numeroCarteira);
    }

    /**
     * Get the variation of the billing portfolio.
     *
     * @return int
     */
    public function getNumeroVariacaoCarteira()
    {
        $this->required('numeroVariacaoCarteira');
        $this->onlyNumber('numeroVariacaoCarteira');

        return $this->int($this->numeroVariacaoCarteira);
    }

    /**
     * Get the number of the bank agency.
     *
     * @return int
     */
    public function getAgenciaBeneficiario()
    {
        $value = $this->removeDigit($this->agenciaBeneficiario);
        $this->onlyNumber('agenciaBeneficiario');

        return $this->int($value);
    }

    /**
     * Get the number of the bank account.
     *
     * @return int
     */
    public function getContaBeneficiario()
    {
        $value = $this->removeDigit($this->contaBeneficiario);
        $this->onlyNumber('contaBeneficiario');

        return $this->int($value);
    }

    /**
     * Get the payee data.
     *
     * @return array
     */
    public function toArray()
    {
        $arr = [];

        foreach (get_class_methods($this) as $method) {
            if (strpos($method, 'get') !== false) {
                $value = $this->{$method}();

                if (is_numeric($value)) {
                    if ($value > 0) {
                        $arr[lcfirst(str_replace(['get'], ['', ''], $method))] = $value;
                    }
                } else {
                    if (!empty($value) || strlen(trim($value)) > 0) {
                        $arr[lcfirst(str_replace(['get'], ['', ''], $method))] = $value;
                    }
                }
            }
        }

        if (isset($this->errors)) {
            return ['errors' => array_unique($this->errors)];
        }

        return $arr;
    }

    public function jsonSerialize()
    {
        return $this->toArray();
    }

    public function formatedData()
    {
        $data = [];

        $data['numeroInscricao'] = $this->formatDocument($this->numeroInscricao);
        $data['razaoSocial'] = $this->razaoSocial;
        $data['endereco'] = $this->endereco;
        $data['cep'] = $this->formatDocument($this->cep);
        $data['cidade'] = $this->cidade;
        $data['bairro'] = $this->bairro;
        $data['uf'] = $this->uf;
        $data['agencia'] = $this->agenciaBeneficiario;
        $data['conta'] = $this->contaBeneficiario;
        $data['carteira'] = $this->numeroCarteira;
        $data['variacaoCarteira'] = $this->numeroVariacaoCarteira;

        return $data;
    }

    /**
     * Format numeroInscricao.
     *
     * @return string|bool
     */
    public function formatDocument($value)
    {
        switch (strlen($value)) {
            case 8:
                // Format ##.###-###
                $result = substr($value, 0, 2) . '.';
                $result .= substr($value, 2, 3) . '-';
                $result .= substr($value, 5, 3) . '';

                return $result;
            case 11:
                // Format ###.###.###-##
                $result = substr($value, 0, 3) . '.';
                $result .= substr($value, 3, 3) . '.';
                $result .= substr($value, 6, 3) . '-';
                $result .= substr($value, 9, 2) . '';

                return $result;
            case 14:
                // Format ##.###.###/####-##
                $result = substr($value, 0, 2) . '.';
                $result .= substr($value, 2, 3) . '.';
                $result .= substr($value, 5, 3) . '/';
                $result .= substr($value, 8, 4) . '-';
                $result .= substr($value, 12, 2) . '';

                return $result;
            default:
                return false;
        }
    }
}
