<?php

/*
   Copyright 2021 Lumiun Tecnologia LTDA - ME

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

namespace Lumiun\CobrancasBB\Classes;

use JsonSerializable;
use Lumiun\CobrancasBB\Middleware\Validation;

class BeneficiarioFinal implements JsonSerializable
{
    use Validation;

    /**
     * Type of registration document.
     * Domain: 1 - CPF | 2 - CNPJ.
     *
     * @var int
     */
    private $tipoInscricao;
    /**
     * Number of registration document.
     *
     * @var int
     */
    private $numeroInscricao;
    /**
     * Guarantee name.
     *
     * @var string
     */
    private $nome;

    public function __construct($data)
    {
        foreach ($data as $key => $value) {
            if (!property_exists(self::class, $key)) {
                continue;
            }
            switch ($key) {
                case 'numeroInscricao':
                    $this->numeroInscricao = $this->cleanDocument($value);
                    $this->tipoInscricao = $this->documentType($this->numeroInscricao);
                    break;
                default:
                    $this->{$key} = $value;
                    break;
            }
        }
    }

    /**
     * Get type of registration document.
     *
     * @return int
     */
    public function getTipoInscricao()
    {
        return $this->tipoInscricao;
    }

    /**
     * Get number of registration document.
     *
     * @return int
     */
    public function getNumeroInscricao()
    {
        $this->required('numeroInscricao', 'CPF/CNPJ');
        $this->documentValidation($this->tipoInscricao, $this->numeroInscricao);

        return $this->int($this->numeroInscricao);
    }

    /**
     * Get guarantee name.
     *
     * @return string
     */
    public function getNome()
    {
        $this->required('nome');
        $this->minLength(5, 'nome');
        $this->maxLength(30, 'nome');

        return $this->string($this->nome);
    }

    /**
     * Get the guarantee data.
     *
     * @return array
     */
    public function toArray()
    {
        $arr = [];

        foreach (get_class_methods($this) as $method) {
            if (strpos($method, 'get') !== false) {
                $value = $this->{$method}();

                if (is_numeric($value)) {
                    if ($value > 0) {
                        $arr[lcfirst(str_replace(['get'], ['', ''], $method))] = $value;
                    }
                } else {
                    if (!empty($value) || strlen(trim($value)) > 0) {
                        $arr[lcfirst(str_replace(['get'], ['', ''], $method))] = $value;
                    }
                }
            }
        }

        if (isset($this->errors)) {
            return ['errors' => array_unique($this->errors)];
        }

        return $arr;
    }

    public function jsonSerialize()
    {
        return $this->toArray();
    }
}
