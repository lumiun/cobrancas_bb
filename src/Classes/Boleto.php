<?php

/*
   Copyright 2021 Lumiun Tecnologia LTDA - ME

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

namespace Lumiun\CobrancasBB\Classes;

use DateInterval;
use DateTimeZone;
use JsonSerializable;
use DateTimeImmutable;
use Lumiun\CobrancasBB\Middleware\Validation;

class Boleto implements JsonSerializable
{
    use Validation;

    /**
     * Código da Modalidade
     * Domínio: 1 - Simples; 4 - Vinculada.
     *
     * @var int
     */
    private $codigoModalidade = 1;

    /**
     * Data de Emissao.
     *
     * @var DateTimeImmutable
     */
    protected $dataEmissao;

    /**
     * Data de Vencimento.
     *
     * @var DateTimeImmutable
     */
    protected $dataVencimento;

    /**
     * Valor Original.
     *
     * @var string|float
     */
    protected $valorOriginal;

    /**
     * Indicador de Aceite para Títulos vencidos
     * Domínio: S - Sim; N - Nao.
     *
     * @var string
     */
    private $indicadorAceiteTituloVencido = 'S';

    /**
     * Quantidade de dias para pagamento vencido.
     *
     * @var int
     */
    private $numeroDiasLimiteRecebimento = 60;

    /**
     * Código do Aceite
     * Domínio: A - Aceito; N - Nao aceito.
     *
     * @var string
     */
    protected $codigoAceite = 'N';

    /**
     * Código do tipo do título.
     * Domínio: 1- CHEQUE; 2- DUPLICATA MERCANTIL;
     * 3- DUPLICATA MTIL POR INDICACAO; 4- DUPLICATA DE SERVICO;
     * 5- DUPLICATA DE SRVC P/INDICACAO; 6- DUPLICATA RURAL;
     * 7- LETRA DE CAMBIO; 8- NOTA DE CREDITO COMERCIAL;
     * 9- NOTA DE CREDITO A EXPORTACAO; 10- NOTA DE CREDITO INDULTRIAL;
     * 11- NOTA DE CREDITO RURAL; 12- NOTA PROMISSORIA; 13- NOTA PROMISSORIA RURAL;
     * 14- TRIPLICATA MERCANTIL; 15- TRIPLICATA DE SERVICO; 16- NOTA DE SEGURO;
     * 17- RECIBO; 18- FATURA; 19- NOTA DE DEBITO; 20- APOLICE DE SEGURO;
     * 21- MENSALIDADE ESCOLAR; 22- PARCELA DE CONSORCIO; 23- DIVIDA ATIVA DA UNIAO;
     * 24- DIVIDA ATIVA DE ESTADO; 25- DIVIDA ATIVA DE MUNICIPIO; 31- CARTAO DE CREDITO;
     * 32- BOLETO PROPOSTA; 33- BOLETO APORTE;  99- OUTROS.
     *
     * @var int
     */
    protected $codigoTipoTitulo = 4;

    /**
     * Descricao do tipo do título.
     *
     * @var string
     */
    protected $descricaoTipoTitulo = 'DS';

    /**
     * Indicador de recebimento parcial
     * Domínio: S - Sim; N - Nao.
     *
     * @var string
     */
    private $indicadorPermissaoRecebimentoParcial = 'N';

    /**
     * Numero do Título do Beneficiário.
     * Max 10 caracteres.
     *
     * @var string|int
     */
    protected $numeroTituloBeneficiario;

    /**
     * Campo de Utilizacao do Beneficiário.
     *
     * @var string
     */
    protected $campoUtilizacaoBeneficiario = '';

    /**
     * Numero do Título do Cliente.
     *
     * @var string
     */
    private $numeroTituloCliente;

    /**
     * Mensagem do Boleto.
     *
     * @var string
     */
    protected $mensagemBloquetoOcorrencia = '';

    /**
     * Indicador de recebimento por PIX
     * Domínio: S - QRCode dinâmico; N - sem Pix.
     *
     * OBS: Qualquer dado diferente de "S", será assumido automaticamente
     * o valor de "N".
     *
     * OBS: Conforme regulamentacao do Bacen, é permitido somente para modalidade de cobranca simples
     * entao se for passado a opcao "S" e a modalidade for "4" Vinculada, retornará erro.
     *
     * @var string
     */
    private $indicadorPix = 'N';

    /**
     * Desconto.
     *
     * @var Desconto
     */
    protected $desconto;

    /**
     * @var JurosMora
     */
    protected $jurosMora;

    /**
     * @var Multa
     */
    protected $multa;

    /**
     * @var Beneficiario
     */
    protected $beneficiario;

    public function __construct($data)
    {
        $this->dataEmissao = new DateTimeImmutable('now', new DateTimeZone('America/Sao_Paulo'));
        $this->init($data);
        $this->desconto = new Desconto($data['desconto'] ?? []);
        $this->jurosMora = new JurosMora($data['jurosMora'] ?? []);
        if (isset($data['multa']) && !isset($data['multa']['data'])) {
            $dataMulta = $this->dataVencimento->add(new DateInterval('P1D'));
            $data['multa']['data'] = $dataMulta->format('Y-m-d');
        }
        $this->multa = new Multa($data['multa'] ?? $this->dataVencimento);
    }

    private function init($data)
    {
        foreach ($data as $key => $value) {
            if (!property_exists(self::class, $key)) {
                continue;
            }
            if (is_array($value)) {
                continue;
            }
            switch ($key) {
                case 'dataVencimento':
                    $this->dataVencimento = new DateTimeImmutable($value);
                    break;
                case 'dataEmissao':
                    $this->dataEmissao = new DateTimeImmutable($value);
                    break;
                case 'numeroTituloCliente':
                    $this->numeroTituloCliente = $value;
                    break;
                case 'beneficiario':
                    $this->beneficiario = $value;
                    break;
                default:
                    $this->{$key} = $value;
                    break;
            }
        }
    }

    /**
     * Get the value of codigoModalidade.
     *
     * @return int
     */
    public function getCodigoModalidade()
    {
        return $this->codigoModalidade;
    }

    /**
     * Get data de Emissao.
     *
     * @return string
     */
    public function getDataEmissao()
    {
        return $this->date($this->dataEmissao);
    }

    /**
     * Get data de Vencimento.
     *
     * @return string|bool
     */
    public function getDataVencimento()
    {
        if (!$this->required('dataVencimento', 'data de vencimento')) {
            return false;
        }
        $dataVencimento = $this->date($this->dataVencimento);
        $dataEmissao = $this->date($this->dataEmissao);

        if (strtotime($dataVencimento) < strtotime($dataEmissao)) {
            $this->errors[] = 'A data de vencimento deve ser maior ou igual a data de emissao!';
        }

        return $this->date($this->dataVencimento);
    }

    /**
     * Get valor Original.
     *
     * @return float|bool
     */
    public function getValorOriginal()
    {
        if (!$this->required('valorOriginal', 'valor original')) {
            return false;
        }

        $valorOriginal = $this->float($this->valorOriginal);
        $desconto = $this->desconto->getValor();

        if ($valorOriginal < $desconto) {
            $this->errors[] = 'O valor do desconto nao pode ser maior do que o valor original da cobranca';
        }
        if ($valorOriginal <= 0.01) {
            $this->errors[] = 'O valor da cobranca deve ser maior que R$ 0,01!';
        }

        return $valorOriginal;
    }

    /**
     * Get the value of indicadorAceiteTituloVencido.
     *
     * @return string
     */
    public function getIndicadorAceiteTituloVencido()
    {
        return $this->indicadorAceiteTituloVencido;
    }

    /**
     * Get the value of numeroDiasLimiteRecebimento.
     *
     * @return int
     */
    public function getNumeroDiasLimiteRecebimento()
    {
        return $this->numeroDiasLimiteRecebimento;
    }

    /**
     * Get the value of codigoAceite.
     *
     * @return string
     */
    public function getCodigoAceite()
    {
        return $this->codigoAceite;
    }

    /**
     * Get the value of codigoTipoTitulo.
     *
     * @return int
     */
    public function getCodigoTipoTitulo()
    {
        return $this->codigoTipoTitulo;
    }

    /**
     * Get the value of descricaoTipoTitulo.
     *
     * @return string
     */
    public function getDescricaoTipoTitulo()
    {
        return $this->descricaoTipoTitulo;
    }

    /**
     * Get the value of indicadorPermissaoRecebimentoParcial.
     *
     * @return string
     */
    public function getIndicadorPermissaoRecebimentoParcial()
    {
        return $this->indicadorPermissaoRecebimentoParcial;
    }

    /**
     * Get numero do Título do Beneficiário.
     *
     * @return int|void
     */
    public function getNumeroTituloBeneficiario()
    {
        if (!$this->required('numeroTituloBeneficiario', 'numero da cobranca')) {
            return;
        }

        $this->onlyNumber('numeroTituloBeneficiario', 'numero da cobranca');
        $this->maxLength(10, 'numeroTituloBeneficiario', 'numero da cobranca');

        $numero = $this->cleanDocument($this->numeroTituloBeneficiario);

        return $this->int($numero);
    }

    /**
     * Get campo de Utilizacao do Beneficiário.
     *
     * @return string
     */
    public function getCampoUtilizacaoBeneficiario()
    {
        $campoUtilizacaoBeneficiario = filter_var($this->campoUtilizacaoBeneficiario, FILTER_SANITIZE_SPECIAL_CHARS);

        if (preg_match('/[^a-zA-Z ]+/', $campoUtilizacaoBeneficiario)) {
            $this->errors[] = 'O campo de utilizacao do beneficiario aceita somente caracteres de A-Z sem 
                acentuacao e em letras maiusculas!';
        }

        $this->maxLength(30, 'campoUtilizacaoBeneficiario', 'de utilizacao do beneficiario');

        return strtoupper($campoUtilizacaoBeneficiario);
    }

    /**
     * Get numero do Título do Cliente.
     *
     * @return string|array
     */
    public function getNumeroTituloCliente()
    {
        if (!$this->numeroTituloCliente) {
            $this->numeroTituloCliente = $this->generateNumeroTituloCliente();
        }

        if (!$this->exactLength(20, 'numeroTituloCliente', 'numero do titulo')) {
            return ['errors' => $this->errors];
        }

        return $this->numeroTituloCliente;
    }

    /**
     * Get mensagem do Boleto.
     *
     * @return string
     */
    public function getMensagemBloquetoOcorrencia()
    {
        $this->maxLength(165, 'mensagemBloquetoOcorrencia', 'mensagem do boleto');

        return $this->string($this->mensagemBloquetoOcorrencia);
    }
    /**
     * Get the valor of indicadorPix
     *
     * @return string|array
     */
    public function getIndicadorPix()
    {
        $domain = [
            'S',
            'N',
        ];

        if ($this->getCodigoModalidade() !== 1) {
            $this->errors[] = 'Conforme regulamentacao do Bacen, o pagamento por PIX 
                e permitido somente para modalidade de cobranca simples!';

            return ['errors' => $this->errors];
        }

        if (!$this->exactLength(1, 'indicadorPix', 'pagamento por PIX')) {
            return ['errors' => $this->errors];
        }

        $this->domain(
            $domain,
            'indicadorPix',
            'pagamento por PIX',
            'O indicador de pagamento por PIX so pode ser "S" - QRCode dinamico; "N" - sem Pix.'
        );

        return $this->indicadorPix;
    }

    /**
     * Get the value of desconto.
     *
     * @return array|bool
     */
    public function getDesconto()
    {
        $desconto = $this->desconto->toArray();

        if (isset($desconto['errors'])) {
            $this->errors = array_merge($this->errors, $desconto['errors']);

            return false;
        }

        return $desconto;
    }

    /**
     * Get the value of jurosMora.
     *
     * @return array|bool
     */
    public function getJurosMora()
    {
        $jurosMora = $this->jurosMora->toArray();

        if (isset($jurosMora['errors'])) {
            $this->errors = array_merge($this->errors, $jurosMora['errors']);

            return false;
        }

        return $jurosMora;
    }

    /**
     * Get the value of multa.
     *
     * @return array|bool
     */
    public function getMulta()
    {
        $multa = $this->multa->toArray();

        if (isset($multa['errors'])) {
            $this->errors = array_merge($this->errors, $multa['errors']);

            return false;
        }

        return $multa;
    }

    /**
     * Set the instance of Beneficiario.
     *
     * @return self
     */
    public function setBeneficiario($beneficiario)
    {
        $this->beneficiario = $beneficiario;

        return $this;
    }

    /**
     * Get the charge data.
     *
     * @return array
     */
    public function toArray()
    {
        $arr = [];

        foreach (get_class_methods($this) as $method) {
            if (strpos($method, 'get') !== false) {
                $value = $this->{$method}();

                if (is_numeric($value)) {
                    if ($value > 0) {
                        $arr[lcfirst(str_replace(['get'], ['', ''], $method))] = $value;
                    }
                } else {
                    if (!empty($value) || strlen(trim($value)) > 0) {
                        $arr[lcfirst(str_replace(['get'], ['', ''], $method))] = $value;
                    }
                }
            }
        }

        if (isset($this->errors)) {
            return ['errors' => array_unique($this->errors)];
        }

        return $arr;
    }

    public function jsonSerialize()
    {
        return $this->toArray();
    }

    /**
     * Generate the number of numeroTituloCliente.
     *
     * 000 + numeroConvenio + numeroTituloBeneficiario
     *
     * @return string
     */
    private function generateNumeroTituloCliente()
    {
        $prefixo = '000';
        $convenio = strval($this->beneficiario->getNumeroConvenio());
        $numero = str_pad((string) $this->getNumeroTituloBeneficiario(), 10, '0', STR_PAD_LEFT);

        return $prefixo . $convenio . $numero;
    }
}
