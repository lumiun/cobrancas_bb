<?php

/*
   Copyright 2021 Lumiun Tecnologia LTDA - ME

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
?>
<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        <?= $css; ?>
    </style>

    <title>Cobrança BB</title>
</head>

<body>
    <div id="container">
        <div id="header">
            <div id="logo-beneficiario">
                <img src="data:image/png;base64,<?= $logoBeneficiario; ?>">
                <div id="link-beneficiario">
                    <a href="https://www.lumiun.com/">lumiun.com</a>
                </div>
            </div>
            <div id="description">
                <table>
                    <tbody>
                        <tr>
                            <td class="top"><?= $descriptionLine1; ?></td>
                        </tr>
                        <tr>
                            <td class="bottom"><?= $descriptionLine2; ?></td>
                        </tr>
                    </tbody>
                </table>
            </div> <!-- id="description" -->
        </div> <!-- id="header" -->

        <div id="boleto">
            <!-- Linha 1 Recibo do Pagador -->
            <table class="header" border=0 cellspacing="0" cellpadding="0">
                <tbody>
                    <tr>
                        <td>
                            <div id="logo-banco">
                                <img src="data:image/png;base64,<?= $logoBanco; ?>">
                            </div>
                        </td>
                        <td>
                            <div class="field-cod-banco">
                                <?= $codigoBanco; ?>
                            </div>
                        </td>
                        <td class="linha-digitavel">
                            <?= $linhaDigitavel; ?>
                        </td>
                    </tr>
                </tbody>
            </table>
            <!-- Linha 1 Recibo do Pagador -->

            <!-- Linha 2 Recibo do Pagador -->
            <table class="line" cellspacing="0" cellpadding="0">
                <tbody>
                    <tr class="titulos">
                        <td class="rp-pagador-nome">Pagador</td>
                        <td class="rp-pagador-documento">CPF/CNPJ</td>
                        <td class="rp-pagador-endereco">Endereço</td>
                    </tr>

                    <tr class="campos">
                        <td class="rp-pagador-nome">
                            <?= $pagador['nome']; ?>
                        </td>
                        <td class="rp-pagador-documento">
                            <?= $pagador['numeroInscricao']; ?>
                        </td>
                        <td class="rp-pagador-endereco">
                            <?= $pagador['endereco']; ?> - <?= $pagador['bairro']; ?><br>
                            <?= $pagador['cep']; ?> - <?= $pagador['cidade']; ?> - <?= $pagador['uf']; ?>
                        </td>
                    </tr>
                </tbody>
            </table>
            <!-- Linha 2 Recibo do Pagador -->

            <!-- Linha 3 Recibo do Pagador -->
            <!-- <table class="line" cellspacing="0" cellpadding="0">
                <tbody>
                    <tr class="titulos">
                        <td class="rp-avalista">Avalista Nome - CPF/CNPJ</td>
                    </tr>
                    
                    <tr class="campos">
                        <td class="rp-avalista">
                            
                        </td>
                    </tr>
                </tbody>
            </table> -->
            <!-- Linha 3 Recibo do Pagador -->

            <!-- Linha 4 Recibo do Pagador -->
            <table class="line" cellspacing="0" cellPadding="0">
                <tbody>
                    <tr class="titulos">
                        <td class="rp-nosso-numero">Nosso Número</td>
                        <td class="rp-numero-doc">Número do Documento</td>
                        <td class="rp-contrato-cobr">Contrato de Cobrança</td>
                        <td class="rp-vencimento">Vencimento</td>
                        <td class="rp-valor-original">Valor documento</td>
                    </tr>
                    <tr class="campos">
                        <td class="rp-nosso-numero">
                            <?= $nossoNumero; ?>
                        </td>
                        <td class="rp-numero-doc">
                            <?= $numeroDocumento; ?>
                        </td>
                        <td class="rp-contrato-cobr">
                            <?= $contratoCobranca; ?>
                        </td>
                        <td class="rp-vencimento text-lg">
                            <?= $dataVencimento; ?>
                        </td>
                        <td class="rp-valor-original text-lg">
                            R$ <?= $valorOriginal; ?>
                        </td>
                    </tr>
                </tbody>
            </table>
            <!-- Linha 4 Recibo do Pagador -->

            <!-- Linha 5 Recibo do Pagador -->
            <table class="line ln-1" cellspacing="0" cellpadding="0">
                <tbody>
                    <tr class="titulos">
                        <td class="rp-beneficiario-rz-social">Beneficiário</td>
                        <td class="rp-beneficiario-cnpj">CPF/CNPJ</td>
                        <td class="rp-beneficiario-endereco">Endereço</td>
                    </tr>

                    <tr class="campos">
                        <td class="rp-beneficiario-rz-social">
                            <?= $beneficiario['razaoSocial']; ?>
                        </td>
                        <td class="rp-beneficiario-cnpj">
                            <?= $beneficiario['numeroInscricao']; ?>
                        </td>
                        <td class="rp-beneficiario-endereco">
                            <?= $beneficiario['endereco']; ?> - <?= $beneficiario['bairro']; ?><br>
                            <?= $beneficiario['cep']; ?> - <?= $beneficiario['cidade']; ?> - <?= $beneficiario['uf']; ?>
                        </td>
                    </tr>
                </tbody>
            </table>
            <!-- Linha 5 Recibo do Pagador -->

            <!-- Linha 6 Recibo do Pagador -->
            <table class="line ln-2" cellspacing="0" cellpadding="0">
                <tbody>
                    <tr class="campos">
                        <td class="rp-beneficiario-ag-cod">
                            <?= $beneficiario['agencia']; ?> / <?= $beneficiario['conta']; ?>
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="rp-footer">
                <p class="rp-beneficiario-ag-cod">Agência / Código do Beneficiário</p>
                <p class="rp-aut-mec">Autentica&ccedil;&atilde;o mec&acirc;nica</p>
            </div>
            <!-- Linha 6 Recibo do Pagador -->

            <!-- Linha de recorte -->
            <div class="cut">
                <p>Corte na linha pontilhada</p>
            </div>
            <table cellspacing=0 cellpadding=0 width=666 border=0>
                <tbody>
                    <tr>
                        <td class=ct width=666>
                            <div align=right><b class=cp>Ficha de Compensação</b></div>
                        </td>
                    </tr>
                </tbody>
            </table>
            <!-- Linha de recorte -->

            <!-- Linha 1 Ficha de Compensação -->
            <table class="header" border=0 cellspacing="0" cellpadding="0">
                <tbody>
                    <tr>
                        <td>
                            <div id="logo-banco">
                                <img src="data:image/png;base64,<?= $logoBanco; ?>">
                            </div>
                        </td>
                        <td>
                            <div class="field-cod-banco">
                                <?= $codigoBanco; ?>
                            </div>
                        </td>
                        <td class="linha-digitavel">
                            <?= $linhaDigitavel; ?>
                        </td>
                    </tr>
                </tbody>
            </table>
            <!-- Linha 1 Ficha de Compensação -->

            <!-- Linha 2 Ficha de Compensação -->
            <table class="line" cellspacing="0" cellpadding="0">
                <tbody>
                    <tr class="titulos">
                        <td class="fc-local-pagto">Local de Pagamento</td>
                        <td class="fc-vencimento">Vencimento</td>
                    </tr>
                    <tr class="campos">
                        <td class="fc-local-pagto">
                            Pag&aacute;vel em qualquer banco
                        </td>
                        <td class="fc-vencimento text-lg">
                            <?= $dataVencimento; ?>
                        </td>
                    </tr>
                </tbody>
            </table>
            <!-- Linha 2 Ficha de Compensação -->

            <!-- Linha 3 Ficha de Compensação -->
            <table class="line" cellspacing="0" cellpadding="0">
                <tbody>
                    <tr class="titulos">
                        <td class="fc-beneficiario-rz-social">Beneficiário</td>
                        <td class="fc-beneficiario-ag-cod">Ag&ecirc;ncia/C&oacute;digo do Beneficiário</td>
                    </tr>
                    <tr class="campos">
                        <td class="fc-beneficiario-rz-social">
                            <?= $beneficiario['razaoSocial']; ?>
                        </td>
                        <td class="fc-beneficiario-ag-cod">
                            <?= $beneficiario['agencia']; ?> / <?= $beneficiario['conta']; ?>
                        </td>
                    </tr>
                </tbody>
            </table>
            <!-- Linha 3 Ficha de Compensação -->

            <!-- Linha 4 Ficha de Compensação -->
            <table class="line" cellspacing="0" cellpadding="0">
                <tbody>
                    <tr class="titulos">
                        <td class="fc-data-doc">Data do documento</td>
                        <td class="fc-numero-doc">No. documento</td>
                        <td class="fc-contrato-cobr">Contrato de Cobrança</td>
                        <td class="fc-especie-doc">Esp&eacute;cie doc.</td>
                        <td class="fc-aceite">Aceite</td>
                        <td class="fc-data-processamento">Data process.</td>
                        <td class="fc-nosso-numero">Nosso n&uacute;mero</td>
                    </tr>
                    <tr class="campos">
                        <td class="fc-data-doc">
                            <?= $dataEmissao; ?>
                        </td>
                        <td class="fc-numero-doc">
                            <?= $numeroDocumento; ?>
                        </td>
                        <td class="fc-contrato-cobr">
                            <?= $contratoCobranca; ?>
                        </td>
                        <td class="fc-especie-doc">
                            <?= $descricaoTipoTitulo; ?>
                        </td>
                        <td class="fc-aceite">
                            <?= $aceite; ?>
                        </td>
                        <td class="fc-data-processamento">
                            <?= $dataRegistro; ?>
                        </td>
                        <td class="fc-nosso-numero">
                            <?= $nossoNumero; ?>
                        </td>
                    </tr>
                </tbody>
            </table>
            <!-- Linha 4 Ficha de Compensação -->

            <!-- Linha 5 Ficha de Compensação -->
            <table class="line" cellspacing="0" cellPadding="0">
                <tbody>
                    <tr class="titulos">
                        <td class="fc-uso-banco">Uso do banco</td>
                        <td class="fc-carteira">Carteira</td>
                        <td class="fc-especie">Espécie</td>
                        <td class="fc-qtd">Quantidade</td>
                        <td class="fc-xvalor">x Valor</td>
                        <td class="fc-valor-original">(=) Valor documento</td>
                    </tr>
                    <tr class="campos">
                        <td class="fc-uso-banco">&nbsp;</td>
                        <td class="fc-carteira">
                            <?= $beneficiario['carteira']; ?> - <?= $beneficiario['variacaoCarteira']; ?>
                        </td>
                        <td class="fc-especie">
                            R$
                        </td>
                        <td class="fc-qtd">

                        </td>
                        <td class="fc-xvalor">

                        </td>
                        <td class="fc-valor-original text-lg">
                            R$ <?= $valorOriginal; ?>
                        </td>
                    </tr>
                </tbody>
            </table>
            <!-- Linha 5 Ficha de Compensação -->

            <!-- Linha 6 Ficha de Compensação -->
            <table class="line" cellspacing="0" cellpadding="0">
                <tbody>
                    <tr>
                        <td class="last_line" rowspan="6">
                            <table class="line" cellspacing="0" cellpadding="0">
                                <tbody>
                                    <tr class="titulos">
                                        <td class="fc-instrucoes">
                                            Instru&ccedil;&otilde;es (Texto de responsabilidade do cedente)
                                        </td>
                                    </tr>
                                    <tr class="campos">
                                        <td class="fc-instrucoes fc-pb" rowspan="5">
                                            <p>
                                                <?= $campoUtilizacaoBeneficiario; ?>
                                            </p>
                                            <p class="fc-instrucoes-mensagem">
                                                <?= $mensagemBloquetoOcorrencia; ?>
                                            </p>
                                            <p>
                                                <?= $instrucoesPagamento; ?>
                                            </p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <table class="line" cellspacing="0" cellpadding="0">
                                <tbody>
                                    <tr class="titulos">
                                        <td class="fc-desconto">(-) Desconto / Abatimento</td>
                                    </tr>
                                    <tr class="campos">
                                        <td class="fc-desconto">&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <table class="line" cellspacing="0" cellpadding="0">
                                <tbody>
                                    <tr class="titulos">
                                        <td class="fc-outras-deducoes">(-) Outras dedu&ccedil;&otilde;es</td>
                                    </tr>
                                    <tr class="campos">
                                        <td class="fc-outras-deducoes">&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <table class="line" cellspacing="0" cellpadding="0">
                                <tbody>
                                    <tr class="titulos">
                                        <td class="fc-mora-multa">(+) Mora / Multa</td>
                                    </tr>
                                    <tr class="campos">
                                        <td class="fc-mora-multa">&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <table class="line" cellspacing="0" cellpadding="0">
                                <tbody>
                                    <tr class="titulos">
                                        <td class="fc-outros-acrescimos">(+) Outros Acr&eacute;scimos</td>
                                    </tr>
                                    <tr class="campos">
                                        <td class="fc-outros-acrescimos">&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td class="last_line">
                            <table class="line" cellspacing="0" cellpadding="0">
                                <tbody>
                                    <tr class="titulos">
                                        <td class="fc-valor-cobrado">(=) Valor cobrado</td>
                                    </tr>
                                    <tr class="campos">
                                        <td class="fc-valor-cobrado">&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
            <!-- Linha 6 Ficha de Compensação -->

            <!-- Linha 7 Ficha de Compensação -->
            <table class="line ln-1" cellspacing="0" cellPadding="0">
                <tbody>
                    <tr class="titulos">
                        <td class="fc-pagador">Pagador</td>
                    </tr>
                    <tr class="campos">
                        <td class="fc-pagador">
                            <p>
                                <?= $pagador['nome']; ?> - <?= $pagador['numeroInscricao']; ?>
                            </p>
                            <p>
                                <?= $pagador['endereco']; ?> - <?= $pagador['bairro']; ?>
                            </p>
                            <p>
                                <?= $pagador['cep']; ?> - <?= $pagador['cidade']; ?> - <?= $pagador['uf']; ?>
                            </p>
                        </td>
                    </tr>
                </tbody>
            </table>
            <!-- Linha 7 Ficha de Compensação -->

            <!-- Linha 8 Ficha de Compensação -->
            <table class="line" cellspacing="0" cellpadding="0">
                <tbody>
                    <tr class="campos">
                        <td class="fc-avalista">&nbsp;</td>
                    </tr>
                    <tr class="titulos">
                        <td class="fc-avalista" colspan="2">Sacador/Avalista</td>
                        <td class="cod_baixa">C&oacute;d. baixa</td>
                    </tr>
                </tbody>
            </table>
            <div class="fc-footer">
                <p class="fc-aut-mec">Autentica&ccedil;&atilde;o mec&acirc;nica - Ficha de Compensação</p>
            </div>
            <!-- Linha 8 Ficha de Compensação -->

            <!-- Barcode -->
            <?= $barcode; ?>
            <!-- Barcode -->

        </div> <!-- id="boleto" -->
    </div> <!-- id="container" -->
</body>

</html>